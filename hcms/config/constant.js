const PersonalDocumentTypeMetadata = {
    CCCD: "Citizen Identity Card/CCCD",
    CMND: "Identity Card/CMND",
    Passport: "Passport",
    DrivingLicense: "Driving License",
    Visa: "Visa",
    WorkPermit: "Work Permit",
    Other: "Other"
}

const ContractStatusMetadata = {
	New: "New",
	Running: "Running",
	Expired: "Expired",
	Cancelled: "Cancelled"
}

const EmployeeWorkflowState = {
    OnboardOffering: "Onboard Offering",
    OnboardOfferAccepting: "Onboard Offer Accepting",
    OnboardOfferAccepted: "Onboard Offer Accepted",
    OnboardOfferDenied: "Onboard Offer Denied",
    OnboardShowingUp: "Onboard Showing Up",
    OnboardAbsence: "Onboard Absence",
    OnboardInfoFilled: "Onboard Info Filled",
    OnboardContractSigned: "Onboard Contract Signed",
    OnboardInProbation: "Onboard In Probation",
    OnboardCancelled: "Onboard Cancelled",
    InterviewCVAssessment: "Interview CV Assessment",
    InterviewUnderInterview: "Interview Under Interview",
    InterviewPassed: "Interview Passed",
    InterviewRejected: "Interview Rejected"
}

const CandidateSourceMetadata = {
    HiringWebsitePortal: 'Hiring Website Portal',
    SocialMedia: 'Social Media',
    InternalTransfer: 'Internal Transfer',
    HeadhuntAgency: 'Headhunt Agency',
    Referral: 'Referral',
    Rehire: 'Rehire',
    CobrandedActivities: 'Co-branded Activities',
    OnsiteService: 'On-site Service'
}

const ReferralMetadata = {
    InternalReferral: 'Internal Referral',
    ExternalReferral: 'External Referral'
} 

const EmployeeStatusMetadata = {
    Applicant: "Applicant",
    ApplicantJunk: "Applicant Junk",
    Candidate: "Candidate",
    CandidateJunk: "Candidate Junk",
    Preboarding: "Preboarding",
    Onboarding: "Onboarding",
    InProbation: "In Probation",
    Active: "Active",
    OnLeaves: "On Leaves",
    UnderTermination: "Under Termination",
    Offboarding: "Offboarding",
}

const ContractTypeMetadata = {
	Contract: "Contract",
    Decision: "Decision",
    Appendix: "Appendix",
    Agreement: "Agreement"
}

const ContractTermMetadata = {
	Internship: "Internship",
	Probation: "Probation",
	FixedTerm: "Fixed-term",
	Permanent: "Permanent"
}

const ContractApprovalWorkflowState = {
    Pending: "Pending",
    Approved: "Approved",
    Rejected: "Rejected"
}

const DeactivationStateMetadata = {
    Immediately: 'Immediately',
    AfterDate: 'After Date'
}

const InvolveRoleMetadata = {
	NewHire: 'New Hire',
	TalentAcquisition: 'Talent Acquisition Manager',
	CB: 'C&B Manager'
}

const WorkflowActionsMetadata = {
    Shortlist: "Shortlist",
    Pass: "Pass",
    CreateOfferLetter: "Create an offer letter",
    SendOffer: "Send the offer letter",
    ResendOffer: "Resend the offer letter",
    Cancel: "Cancel",
    Absent: "Absent",
    UpdateOfferLetter: "Update Offer Letter",
    ConfirmShowingUp: "Confirm Showing-up",
    SendAgenda: "Send Agenda",
    Submit: "Submit",
    SignContract: "Sign Contract",
    ConfirmInProbation: "Confirm In Probation",
    Reject: "Reject"
}

const Messages = {
    SaveFormFirst: 'Please save the form before taking any actions.',
    IntroCreateOffer: "To create Offer Letter, please provide required candidate information.",
    CreateProbationContract: 'Please create a probation contract.',
    ProbationContractUnderApproval: 'The probation contract is under approval process.',
    ProfileSubmitMessage: 'By submiting your profile, you hereby confirm that all your provided information is correct.',
    SignContractInstruction: 'Please sign both the digital contract and the paper contract.',
    CannotUndoneAction: 'This action cannot be undone.',
    UnsavedTakeAction: 'Are you sure to save and ',
    SavedTakeAction: 'Are you sure to ',
    RequestUpdateInformation: 'Please complete employee profile so that HR Department could proceed to next steps.',
    RemindSendAgenda: 'For smooth onboarding day, please send onboarding agenda to candidate.',
    MissingProbationContract: 'Something went wrong: Cannot find candidate\'s approved primary probation contract.',

    OTPConfirmation: 'OTP Confirmation',
    EnterOTPInEmail: 'Enter the OTP sent to your personal email'
}

const APIPath = {
    RemoveTermination: "hcms.human_capital_management_system.doctype.employee.employee_services.remove_termination",
    CreateLDAPAccount: "hcms.human_capital_management_system.doctype.employee.employee_services.create_ldap_user_account",
    ChangeWorkflowState: "hcms.human_capital_management_system.doctype.employee.employee_services.change_workflow_state",
    CancelProbationContract: "hcms.human_capital_management_system.doctype.employee.employee_services.cancel_probation_contract",
    SendOTPToCandidate: "hcms.human_capital_management_system.doctype.employee.employee_services.send_otp_to_candidate",
    HandleWorkflowAction: "hcms.human_capital_management_system.doctype.employee.employee_services.handle_workflow_action",
    GetListWorkflowAction: "hcms.human_capital_management_system.doctype.employee.employee_services.get_list_workflow_actions",
    GenerateOfferLetterTemplate: "hcms.human_capital_management_system.doctype.employee.employee_services.generate_offer_letter_template",
    GenerateOnboardingAgendaTemplate: "hcms.human_capital_management_system.doctype.employee.employee_services.generate_onboarding_agenda_template",
    GetListEnablePrimaryContract: "hcms.human_capital_management_system.doctype.employee.employee_services.get_list_enable_primary_contract",
    GetListContractAttachments: "hcms.human_capital_management_system.doctype.employee.employee_services.get_list_contract_attachments"
}

const EmailTemplate = {
    OfferLetter: 'offer_letter',
    Agenda: 'agenda'
}

const DELAY_IN_MILLIS = 1500