// Copyright (c) 2022, masterisehomes and contributors
// For license information, please see license.txt

var current_company;

// function update_options_for_authorizing_person_from_db(company_name){
//     frappe.db.get_list(
//         'Business Representative',
//         {
//             fields: ['full_name_display', 'company'],
//             filters: { 
//                 company: company_name
//             }
//         }
//     ).then(records => {
//         list_business_representatives = []
//         records.forEach(element => {
//             list_business_representatives.push(element.full_name_display)
//         });
//         frappe.meta.get_docfield('Authorizing Person', 'authorized_by', company_name).options = list_business_representatives
//     })
// }

// function update_options_for_authorizing_person_from_field(company_name){
//     list_business_representatives = []
//     console.log('update_options_for_authorizing_person_from_field')
//     parent_frm.doc.business_representative.forEach((item) => {
//         list_business_representatives.push(item.full_name_display)
//     })

//     list_business_representatives.forEach((item) => {
//         console.log(`latest BR ${item}`)
//     })

//     frappe.meta.get_docfield('Authorizing Person', 'authorized_by', company_name).options = list_business_representatives


// }

function set_query_for_br_ap(frm){
    frm.set_query("comp_br", "br_ap", function(doc) {
        console.log(doc)
        return {
            filters: {
                company: current_company
            }
        };
    });
}

frappe.ui.form.on('Company', {

    onload: function(frm){
        current_company = frm.doc.name
        set_query_for_br_ap(frm)
    },

    refresh: function(frm){
        current_company = frm.doc.name
        set_query_for_br_ap(frm)
    },

    br_ap_on_form_rendered: function(frm){
        console.log('br_ap_on_form_rendered')
        console.log(current_company)

        set_query_for_br_ap(frm)
    }


});


// frappe.ui.form.on('Authorizing Person',{
    
//     valid_from: function(frm, cdt, cdn){
//         let row = frappe.get_doc(cdt, cdn)
//         let dateNow = frappe.datetime.get_today()

//         /// Valid From must has value
//         if (row.valid_from){
//             if (row.valid_to){
//                 // Throw exception if valid_from > valid_to
//                 if (row.valid_from > row.valid_to){    
//                     frappe.model.set_value(cdt, cdn, 'status', '')
//                     frappe.model.set_value(cdt, cdn, 'valid_from', '')

//                     refresh_field('valid_from')
//                     refresh_field('status')

//                     frappe.throw('Valid From must be less than or equal to Valid To')
//                 }
//                 else{
//                     if ((row.valid_from <= dateNow) && (dateNow <= row.valid_to)){
//                         frappe.model.set_value(cdt, cdn, 'status', 'Active')
//                     }else{
//                         frappe.model.set_value(cdt, cdn, 'status', 'Inactive')
//                     }   
//                     refresh_field('status')
//                 }
//             }else{
//                 if (row.valid_from <= dateNow){
//                     frappe.model.set_value(cdt, cdn, 'status', 'Active')
//                 }else{
//                     frappe.model.set_value(cdt, cdn, 'status', 'Inactive')
//                 }
    
//                 refresh_field('status')
//             }
//         }
//         /// Valid From has no data => reset status to be empty
//         else{
//             frappe.model.set_value(cdt, cdn, 'status', '')
//             refresh_field('status')
//         }      
//     },
    
//     valid_to: function(frm, cdt, cdn){
//         let row = frappe.get_doc(cdt, cdn)
//         let date_now = frappe.datetime.get_today()
//         if (row.valid_from){
//             // throw exception if valid_from > valid_to
//             if (row.valid_from > row.valid_to){

//                 frappe.model.set_value(cdt, cdn, 'valid_to', '')

//                 refresh_field('valid_to')

//                 frappe.throw('Valid To must be the date after Valid From')
//             }
//             else{
//                 if (
//                     ((row.valid_from <= date_now) && (date_now <= row.valid_to))
//                     || ((row.valid_from <= date_now) && (!row.valid_to))){
//                     frappe.model.set_value(cdt, cdn, 'status', 'Active')
//                 }else{
//                     frappe.model.set_value(cdt, cdn, 'status', 'Inactive')
//                 }
//                 refresh_field('status')
//             }
//         }
//     },

// })

// frappe.ui.form.on('Business Representative',{

//     // full_name: function(frm, cdt, cdn){
//     //     let child = locals[cdt][cdn]
//     //     frappe.model.set_value(cdt, cdn, 'company', frm.doc.name);

//     //     frappe.db.get_value('Employee', child.full_name, 'full_name').then((item) => {
//     //         frappe.model.set_value(cdt, cdn, 'full_name_display', item.message.full_name);
//     //         // parent_frm.save()
//     //         update_options_for_authorizing_person_from_field(current_company)
//     //     })
//     // },




//     // business_representative_add: function(frm, cdt, cdn){
//     //     console.log('business_representative_add')
//     // },

//     // business_representative_remove: function(frm, cdt, cdn){
//     //     console.log('business_representative_remove')

//     //     parent_frm.save()
//     //     update_options_for_authorizing_person_from_db(current_company)
//     // },
    
//     valid_from: function(frm, cdt, cdn){
//         let row = frappe.get_doc(cdt, cdn)
//         let dateNow = frappe.datetime.get_today()

//         /// Valid From must has value
//         if (row.valid_from){
//             if (row.valid_to){
//                 // Throw exception if valid_from > valid_to
//                 if (row.valid_from > row.valid_to){    
//                     frappe.model.set_value(cdt, cdn, 'status', '')
//                     frappe.model.set_value(cdt, cdn, 'valid_from', '')

//                     refresh_field('valid_from')
//                     refresh_field('status')

//                     frappe.throw('Valid From must be less than or equal to Valid To')
//                 }
//                 else{
//                     if ((row.valid_from <= dateNow) && (dateNow <= row.valid_to)){
//                         frappe.model.set_value(cdt, cdn, 'status', 'Active')
//                     }else{
//                         frappe.model.set_value(cdt, cdn, 'status', 'Inactive')
//                     }   
//                     refresh_field('status')
//                 }
//             }else{
//                 if (row.valid_from <= dateNow){
//                     frappe.model.set_value(cdt, cdn, 'status', 'Active')
//                 }else{
//                     frappe.model.set_value(cdt, cdn, 'status', 'Inactive')
//                 }
    
//                 refresh_field('status')
//             }
//         }
//         /// Valid From has no data => reset status to be empty
//         else{
//             frappe.model.set_value(cdt, cdn, 'status', '')
//             refresh_field('status')
//         }      
//     },
    
//     valid_to: function(frm, cdt, cdn){
//         let row = frappe.get_doc(cdt, cdn)
//         let dateNow = frappe.datetime.get_today()
//         if (row.valid_from){
//             // throw exception if valid_from > valid_to
//             if (row.valid_from > row.valid_to){

//                 frappe.model.set_value(cdt, cdn, 'valid_to', '')

//                 refresh_field('valid_to')

//                 frappe.throw('Valid To must be the date after Valid From')
//             }
//             else{
//                 if (
//                     ((row.valid_from <= dateNow) && (dateNow <= row.valid_to))
//                     || ((row.valid_from <= dateNow) && (!row.valid_to))){
//                     frappe.model.set_value(cdt, cdn, 'status', 'Active')
//                 }else{
//                     frappe.model.set_value(cdt, cdn, 'status', 'Inactive')
//                 }
//                 refresh_field('status')
//             }
//         }
        
//     }
// })