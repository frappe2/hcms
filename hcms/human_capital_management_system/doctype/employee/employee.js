// Copyright (c) 2022, masterisehomes and contributors
// For license information, please see license.txt

var back_from_contract = false
var primary_contract
var has_primary_contract = false

var list_active_contract = []
var list_contract = []

var selected_permanent_country;
var selected_permanent_province;
var selected_permanent_district;
var selected_current_country;
var selected_current_province;
var selected_current_district;

var list_actions = [];
var has_enable_primary_approved_contract = false;
var primary_enable_probation_contract = null;
var should_display_remind_sending_agenda = true;
var current_intro_message = null;

/// FUNCTIONS FOR ADDRESS ///

function set_query_for_province(frm, current_country, province_field) {

    frm.set_query(province_field, () => {
        return {
            filters: {
                country: current_country
            }
        }
    })
}

function set_query_for_district(frm, current_province, district_field) {
    frm.set_query(district_field, () => {
        return {
            filters: {
                province: current_province
            }
        }
    })
}

function set_query_for_ward(frm, current_district, ward_field) {
    frm.set_query(ward_field, () => {
        return {
            filters: {
                district: current_district
            }
        }
    })
}

/// FULL NAME ///
function update_full_name(frm) {
    let first_name = frm.doc.first_name ? frm.doc.first_name.trim() : ''
    let middle_name = frm.doc.middle_name ? frm.doc.middle_name.trim() : ''
    let last_name = frm.doc.last_name ? frm.doc.last_name.trim() : ''
    if (frm.doc.nationality == 'Vietnam') {
        frm.set_value('full_name', last_name.concat(' ', middle_name, ' ', first_name))
    } else {
        frm.set_value('full_name', first_name.concat(' ', middle_name, ' ', last_name))
    }
}


function get_primary_contract(list_contract) {
    if (list_contract) {
        for (let i = 0; i < list_contract.length; i++) {
            if (list_contract[i].is_primary == 1 && list_contract[i].status == ContractStatusMetadata.Running) {
                return list_contract[i]
            }
        }
    }
    return null
}


function get_upcoming_probation_contract(list_contract) {
    if (list_contract && list_contract.length == 1) {
        if (list_contract[0].is_primary
            && ([ContractStatusMetadata.New, ContractStatusMetadata.Running].includes(list_contract[0].status))
            && (list_contract[0].workflow_state == ContractApprovalWorkflowState.Approved)) {
            return list_contract[0]
        }
    }
    return null
}

function get_list_active_contract(list_contract) {
    list_active_contract = []

    for (let contract of list_contract) {
        if (contract.enable) {
            list_active_contract.push(contract)
        }
    }
    return list_active_contract
}


/// TERMINATION PROCESS ///
async function remove_termination_process(frm) {
    frm.set_value('terminate_last_working_date', null)
    frm.set_value('business_acc_deact_date', null)
    frm.set_value('termination_reason', null)
    frm.set_value('detailed_reason', null)

    await Promise.resolve(frm.save())

    let result_status = await Promise.resolve(
        frappe.call({
            method: APIPath.RemoveTermination,
            args: {
                employee: frm.doc.name,
            },
        }),
    )
    
    frm.save()
}

function update_cancel_termination_process_button(frm) {
    if (frm.doc.employee_contract_status == EmployeeStatusMetadata.UnderTermination) {
        frm.toggle_display(['remove_termination_btn'], true)
    } else {
        frm.toggle_display(['remove_termination_btn'], false)
    }
}

function update_terminate_last_working_date(frm, contract_end_date) {
    if (!(frm.doc.terminate_last_working_date)) {
        frm.set_value('terminate_last_working_date', contract_end_date)
    }
}

function get_list_contract(frm) {
    return frappe.db.get_list(
        hcms_core.constants.DoctypeName.Contract,
        {
            fields: ['name', 'company', 'department', 'job_position', 'position_band', 'start_date', 'end_date', 'status', 'type', 'term', 'is_primary', 'enable'],
            filters: {
                employee: frm.doc.name,
            }
        }
    )
}

function update_contracts_info(frm) {

    console.log('init form')

    list_active_contract = []
    list_contract = []

    get_list_contract(frm).then(records => {

        list_contract = records

        list_active_contract = get_list_active_contract(list_contract)
        let probation_contract = get_upcoming_probation_contract(list_active_contract)
        if (probation_contract) {
            primary_contract = probation_contract
        } else {
            primary_contract = get_primary_contract(list_active_contract)
        }

        has_primary_contract = primary_contract != null ? true : false

        console.log(`has_primary_contract = ${has_primary_contract}`)

    })
}


function handle_province_quick_entry(frm, province_type, provided_country) {
    frm.get_docfield(province_type).get_route_options_for_new_doc = function (field) {
        return {
            "country": provided_country
        }
    }
}


function handle_district_quick_entry(frm, district_type, provided_province) {
    frm.get_docfield(district_type).get_route_options_for_new_doc = function (field) {
        return {
            "province": provided_province
        }
    }
}

function handle_ward_quick_entry(frm, ward_type, provided_district) {
    frm.get_docfield(ward_type).get_route_options_for_new_doc = function (field) {
        return {
            "district": provided_district
        }
    }
}

function create_account(frm, callback_fn) {
    try {
        frappe.call({
            method: APIPath.CreateLDAPAccount,
            args: {
                employee: frm.doc.name
            },
            callback: function (r) {
                if (r.message == false) {
                    frappe.msgprint({
                        title: __('Notification'),
                        indicator: 'red',
                        message: __('An error is occurred. Please try again later')
                    })
                } else if (r.message == true) {
                    if (frappe.user.has_role(InvolveRoleMetadata.TalentAcquisition)) {
                        console.log("is Talent Acquisition Manager")
                        frm.reload_doc()
                    }

                    callback_fn()

                }
            }
        })
    } catch (err) {
        console.log(err)
    }

}


/// HANDLE WORKFLOW ACTIONS ///
function change_workflow_state(frm, new_state, should_notify_manager = false, notify_message, on_state_changed_callback) {

    frappe.call({
        method: APIPath.ChangeWorkflowState,
        args: {
            employee: frm.doc.name,
            new_state: new_state,
            should_notify_manager: should_notify_manager ? "1" : "0",
            notify_message: notify_message
        },
        callback: function (_) {
            if (on_state_changed_callback !== null && on_state_changed_callback !== undefined) {
                on_state_changed_callback()
            }
            frm.reload_doc()
        }
    })
}

/// DIALOGS ///
function show_success_dialog(message) {
    setTimeout(function () {
        frappe.show_alert({
            message: message,
            indicator: 'green'
        }, 5)
    }, DELAY_IN_MILLIS)
}

function show_error_dialog(frm, message) {
    frappe.show_alert({
        message: message,
        indicator: 'red'
    }, 5)
}

function show_warning_dialog(frm, title, message, callback) {

    frappe.warn(
        title,
        message,
        () => {
            console.log('warning dialog callback')
            callback()
        },
        'Continue',
        true
    )
}

function show_process_dialog() {
    frappe.msgprint({
        title: __('Processing'),
        indicator: 'blue',
        message: __('Please wait...')
    })


    // console.log('show process dialog')
}


function show_information_dialog(title, message) {
    frappe.msgprint({
        title: title,
        indicator: 'red',
        message: message
    })
}

function close_dialog() {

    setTimeout(function () {
        cur_dialog.hide()
    }, DELAY_IN_MILLIS)

    // // setInterval is used to make sure that the processing dialog is shown before being closed
    // let dialog_interval = setInterval(function(){
    //     console.log('dialog interval called')
    //     if (cur_dialog !== null){
    //         cur_dialog.hide()
    //         clearInterval(dialog_interval)
    //     }
    // }, 1000)

    // return dialog_interval
}


async function show_contract(frm, on_next_step_changed) {
    console.log('primary_enable_probation_contract')
    console.log(primary_enable_probation_contract)
    if (await has_valid_contract(frm)) {
        let d = new frappe.ui.Dialog({
            title: primary_enable_probation_contract.name,
            fields: [
                {
                    fieldname: 'pdf_view',
                    fieldtype: 'HTML'
                }
            ],
            primary_action_label: "Sign",
            primary_action(values) {
                d.hide()
                on_next_step_changed()
            }
        });

        /// time is added as a param to always load new file
        d.set_value('pdf_view', `<object data="${primary_enable_probation_contract.pdf_file}" width="100%" height="700"></object>`)
        d.show()
        $(d.$wrapper.find('.modal-dialog')).addClass('modal-lg')
    }

}


function show_intro(frm, message, color) {
    if (current_intro_message !== message) {
        current_intro_message = message
        frm.set_intro(message, color)
    }
}



/// END OF DIALOGS ///

function update_employee_status(frm) {
    let current_state = frm.doc.workflow_state

    switch (current_state) {
        case EmployeeWorkflowState.InterviewCVAssessment:
            if (frm.doc.last_name && frm.doc.middle_name && frm.doc.first_name && frm.doc.private_email) {
                frm.set_value('status', EmployeeStatusMetadata.Applicant)
            }
            break
    }

}

/// WORKFLOW BUTTONS ///
async function get_list_enable_primary_probation_contract(frm) {
    console.log('get_list_enable_primary_probation_contract')
    return await Promise.resolve(
        frappe.db.get_list(
            // 'Contract',
            hcms_core.constants.DoctypeName.Contract,
            {
                fields: ['name', 'company', 'department', 'job_position', 'position_band', 'start_date', 'end_date', 'status', 'type', 'term', 'is_primary', 'enable', 'workflow_state', 'pdf_file'],
                filters: {
                    employee: frm.doc.name,
                    status: ["in", [ContractStatusMetadata.New, ContractStatusMetadata.Running]],
                    type: ContractTypeMetadata.Contract,
                    term: ContractTermMetadata.Probation,
                    is_primary: 1,
                    enable: 1
                }
            }
        )
    )
}

async function get_list_enable_primary_contract(frm) {
    return await Promise.resolve(
        frappe.call({
            method: APIPath.GetListEnablePrimaryContract,
            args: {
                emp: frm.doc.name,
            },
        })
    )
}

async function has_valid_probation_contract(frm) {
    let list_contract = await get_list_enable_primary_probation_contract(frm)
    console.log('list contract')
    console.log(list_contract)
    if (list_contract && list_contract.length == 1) {
        primary_enable_probation_contract = list_contract[0]

        return true
    }
    return false
}

async function has_valid_contract(frm) {
    let list_contract_message = await get_list_enable_primary_contract(frm)
    let list_contract = list_contract_message.message
    if (list_contract) {
        if (list_contract.length == 1 && list_contract[0].term == ContractTermMetadata.Probation) {
            primary_enable_probation_contract = list_contract[0]
            return true
        }
    }
    return false
}

async function get_current_primary_contract_situation(frm) {
    let list_contract_message = await get_list_enable_primary_contract(frm)
    let list_contract = list_contract_message.message

    let current_primary_contract = null
    let has_current_primary_contract = false

    if (list_contract) {
        /// Find a running contract -> #1 priority
        for (let i = 0; i < list_contract.length; i++) {
            if (list_contract[i].term == ContractTermMetadata.Probation) {
                current_primary_contract = list_contract[i]
                has_current_primary_contract = true
                return { current_primary_contract, has_current_primary_contract }
            } else {
                if (list_contract[i].status == ContractStatusMetadata.Running) {

                }
            }
        }

        /// Otherwise, find a running contract
    }
}

function cancel_probation_contract(frm) {
    if (primary_enable_probation_contract) {
        console.log('primary_enable_probation_contract name')
        console.log(primary_enable_probation_contract.name)
        frappe.call({
            method: APIPath.CancelProbationContract,
            args: {
                contract_name: primary_enable_probation_contract.name
            },
        })
    }

}

function get_dialog_workflow_message(frm, core_message) {
    if (frm.is_dirty()) {
        return Messages.UnsavedTakeAction.concat(core_message)
    }
    return Messages.SavedTakeAction.concat(core_message)
}

function send_otp_to_candidate(frm, action) {
    frappe.call({
        method: APIPath.SendOTPToCandidate,
        args: {
            employee: frm.doc.name,
            action: action,
            document_id: primary_enable_probation_contract.name
        },
    })
}


function handle_workflow_action(frm, action, callback_fn) {
    frappe.call({
        method: APIPath.HandleWorkflowAction,
        args: {
            employee: frm.doc.name,
            action: action,
        },
        callback: async function (r) {
            console.log('handle_workflow_action')
            console.log(r.message)
            if (r.message && r.message.length === 2) {
                if (r.message[0]) {
                    if (callback_fn !== null && callback_fn !== undefined) {
                        await callback_fn()
                    }
                    console.log('reload doc')
                    frm.reload_doc()
                } else {
                    close_dialog()
                    show_error_dialog(frm, r.message[1])
                }
            }

        }
    })
}


function submit_otp_contract(frm, action, otp, callback_fn) {
    console.log('submit_otp_contract')
    console.log(otp)
    console.log(action)
    frappe.call({
        method: APIPath.HandleWorkflowAction,
        args: {
            employee: frm.doc.name,
            action: action,
            submitted_otp: otp
        },
        callback: async function (r) {
            console.log('handle_workflow_action')
            console.log(r.message)
            if (r.message && r.message.length === 2) {
                if (r.message[0]) {
                    if (callback_fn !== null && callback_fn !== undefined) {
                        await callback_fn()
                    }
                    console.log('reload doc')
                    frm.reload_doc()
                } else {
                    close_dialog()
                    show_error_dialog(frm, r.message[1])
                }
            }

        }
    })
}


function show_otp_confirmation_dialog(frm, on_otp_submitted) {
    let dialog = new frappe.ui.Dialog({

        title: __(Messages.OTPConfirmation),
        fields: [
            {
                'label': `${__(Messages.EnterOTPInEmail)}:`,
                'fieldname': 'otp',
                'fieldtype': 'Data',
                'reqd': 1,
            },
        ],
        primary_action_label: 'Submit',
        primary_action(values) {
            on_otp_submitted(values.otp)
            dialog.hide()
        }
    })

    dialog.show()
}


function create_workflow_button(frm, action) {
    frm.add_custom_button(action.action, async () => {
        let title = ''
        let message = ''
        console.log('next state')
        console.log(action.next_state)

        var callback_fn = async () => {
            console.log('employee callback fn execute called')
            await hcms_core.utils.execute(frm, () => {
                show_process_dialog()
                handle_workflow_action(frm, action.action, () => {
                    close_dialog()
                })
            })
        }

        switch (action.action) {
            case WorkflowActionsMetadata.Shortlist:
                title = `CV Assessment`
                message = get_dialog_workflow_message(frm, `confirm that ${frm.doc.full_name} pass the CV assessment stage?`)
                show_warning_dialog(frm, title, message, callback_fn)
                break
            case WorkflowActionsMetadata.Pass:
                title = `Interview`
                message = get_dialog_workflow_message(frm, `confirm that ${frm.doc.full_name} passed the interview stage?`)
                show_warning_dialog(frm, title, message, callback_fn)
                break

            case WorkflowActionsMetadata.Reject:
                title = `Reject Applicant`
                message = get_dialog_workflow_message(frm, `reject ${frm.doc.full_name}?`)
                show_warning_dialog(frm, title, message, callback_fn)
                break

            case WorkflowActionsMetadata.CreateOfferLetter:
                if (can_create_job_offer(frm)) {
                    console.log('can create job offer')
                    await hcms_core.utils.execute(frm, () => {
                        back_from_contract = true
                        frappe.set_route("Form", hcms_core.constants.DoctypeName.Contract, {
                            'employee': frm.doc.name,
                            'company': frm.doc.company,
                        })
                    })

                } else {
                    console.log('cannot create job offer')
                    show_information_dialog(
                        'Cannot create an offer letter',
                        'Please fill in all the required fields',
                    )
                }
                break

            case WorkflowActionsMetadata.SendOffer:
                title = `Send offer`
                console.log('pdf link')
                console.log(primary_enable_probation_contract.pdf_file)

                callback_fn = async () => {
                    await hcms_core.utils.execute(frm, () => {
                        compose_mail(
                            frm,
                            frm.doc,
                            frm.doc.private_email,
                            '',
                            EmailTemplate.OfferLetter,
                            () => {
                                show_process_dialog()
                                handle_workflow_action(frm, action.action, () => {
                                    console.log('hide process dialog')
                                    close_dialog()
                                    show_success_dialog('Email sent successfully.')
                                })
                            },
                            [primary_enable_probation_contract.pdf_file],
                        )
                    })
                }

                await callback_fn()
                break

            case WorkflowActionsMetadata.ResendOffer:
                title = `Resend offer`

                callback_fn = async () => {
                    await hcms_core.utils.execute(frm, () => {
                        compose_mail(
                            frm,
                            frm.doc,
                            frm.doc.private_email,
                            '',
                            EmailTemplate.OfferLetter,
                            () => {
                                show_process_dialog()
                                handle_workflow_action(frm, action.action, () => {
                                    console.log('hide process dialog')
                                    close_dialog()
                                    show_success_dialog('Email sent successfully.')
                                })
                            },
                            [primary_enable_probation_contract.pdf_file],
                        )
                    })
                }
         
                await callback_fn()
                break

            case WorkflowActionsMetadata.Absent:
                title = 'Confirm Absence'
                message = get_dialog_workflow_message(frm, `confirm that ${frm.doc.full_name} is absent on the onboarding date?`)
                show_warning_dialog(frm, title, message, callback_fn)
                break

            case WorkflowActionsMetadata.UpdateOfferLetter:
                title = 'Update offer letter'
                message = get_dialog_workflow_message(frm, `update the offer letter of ${frm.doc.full_name}?`)
                show_warning_dialog(frm, title, message, callback_fn)
                break

            case WorkflowActionsMetadata.ConfirmShowingUp:
                title = 'Confirm showing-up'
                message = get_dialog_workflow_message(frm, `confirm that ${frm.doc.full_name} is showing up on the onboarding date?`)
                show_warning_dialog(frm, title, message, callback_fn)
                break

            case WorkflowActionsMetadata.SendAgenda:
                title = `Send Agenda`
                callback_fn = async () => {
                    await hcms_core.utils.execute(frm, () => {
                        compose_mail(
                            frm,
                            frm.doc,
                            frm.doc.private_email,
                            // `Dear ${frm.doc.full_name},<br><br>Your incoming onboarding date details<br>Date: ${primary_enable_probation_contract.start_date}<br>Time:<br>Place:<br><br>Best regards,`,
                            '',
                            EmailTemplate.Agenda,
                            () => {
                                show_process_dialog()
                                handle_workflow_action(frm, action.action, () => {
                                    console.log('hide process dialog')
                                    close_dialog()
                                    show_success_dialog('Email sent successfully.')
                                })
                            },
                        )
                    })
                }
          
                await callback_fn()
                break


            case WorkflowActionsMetadata.Submit:
                title = 'Submit Profile'
                message = get_dialog_workflow_message(frm, `submit this profile?<br>${__(Messages.ProfileSubmitMessage)}`)
                show_warning_dialog(frm, title, message, callback_fn)
                break

            case WorkflowActionsMetadata.SignContract:
                title = 'Sign Probation Contract'
                message = __(Messages.SignContractInstruction)

                show_contract(frm, () => {
                    send_otp_to_candidate(frm, action.action)
                    show_otp_confirmation_dialog(frm, function (otp) {
                        show_process_dialog()
                        submit_otp_contract(frm, action.action, otp, () => {
                            close_dialog()
                        })
                    })
                })

                break

            case WorkflowActionsMetadata.ConfirmInProbation:
                title = 'In Probation Confirmation'
                message = `Are you sure to confirm that ${frm.doc.full_name} is officially in probation?`
                show_warning_dialog(frm, title, message, callback_fn)
                break

            case WorkflowActionsMetadata.Cancel:
                title = 'Cancel profile'
                message = `Are you sure to cancel profile ${frm.doc.name} - ${frm.doc.full_name}?`
                show_warning_dialog(frm, title, message, callback_fn)
                break

        }
    })
}


function create_list_action_buttons(frm) {
    console.log('create_list_action_buttons action')
    list_actions.forEach(action => {
        console.log(action.action)

        switch (action.action) {
            case WorkflowActionsMetadata.Shortlist:
                create_workflow_button(frm, action)
                break
            case WorkflowActionsMetadata.Pass:
                create_workflow_button(frm, action)
                break

            case WorkflowActionsMetadata.Reject:
                create_workflow_button(frm, action)
                break

            case WorkflowActionsMetadata.CreateOfferLetter:
                console.log('has_enable_primary_approved_contract')
                console.log(has_enable_primary_approved_contract)
                if (has_enable_primary_approved_contract === false) {

                    // change_mandatory_addresses(frm, 1)
                    change_mandatory_personal_document(frm, 1)
                    console.log('primary_enable_probation_contract create offer letter')
                    console.log(primary_enable_probation_contract)
                    if (primary_enable_probation_contract === null || primary_enable_probation_contract === undefined) {
                        show_intro(frm, __(Messages.IntroCreateOffer), 'orange')
                        create_workflow_button(frm, action)
                    } else {
                        switch (primary_enable_probation_contract.workflow_state) {
                            case ContractApprovalWorkflowState.Rejected:
                                show_intro(frm, __(Messages.IntroCreateOffer), 'orange')
                                create_workflow_button(frm, action)
                                break
                            case ContractApprovalWorkflowState.Pending:
                                show_intro(frm, __(Messages.ProbationContractUnderApproval), 'orange')
                                break
                        }
                    }
                }
                break

            case WorkflowActionsMetadata.SendOffer:
                create_workflow_button(frm, action)
                break

            case WorkflowActionsMetadata.ResendOffer:
                frm.remove_custom_button(WorkflowActionsMetadata.SendOffer)
                create_workflow_button(frm, action)
                break

            case WorkflowActionsMetadata.UpdateOfferLetter:
                create_workflow_button(frm, action)
                break

            case WorkflowActionsMetadata.Absent:
                if (primary_enable_probation_contract) {
                    if (primary_enable_probation_contract.status == ContractStatusMetadata.Running) {
                        create_workflow_button(frm, action)
                    }
                }
                // create_workflow_button(frm, action)
                break

            case WorkflowActionsMetadata.ConfirmShowingUp:
                if (primary_enable_probation_contract) {
                    if (primary_enable_probation_contract.status == ContractStatusMetadata.Running) {
                        create_workflow_button(frm, action)
                    }
                } else {
                    show_intro(frm, __(Messages.MissingProbationContract), 'orange')
                }

                // create_workflow_button(frm, action)

                break

            case WorkflowActionsMetadata.SendAgenda:
                if (primary_enable_probation_contract) {
                    if (primary_enable_probation_contract.status == ContractStatusMetadata.New) {
                        if (should_display_remind_sending_agenda) {
                            show_intro(frm, __(Messages.RemindSendAgenda), 'orange')
                            should_display_remind_sending_agenda = false
                        }
                        create_workflow_button(frm, action)
                    }
                } else {
                    show_intro(frm, __(Messages.MissingProbationContract), 'orange')
                }
                break

            case WorkflowActionsMetadata.Submit:
                if (frappe.user.has_role(InvolveRoleMetadata.Candidate)) {
                    show_intro(frm, __(Messages.RequestUpdateInformation), 'orange')
                }

                create_workflow_button(frm, action)
                break

            case WorkflowActionsMetadata.SignContract:
                create_workflow_button(frm, action)
                break

            case WorkflowActionsMetadata.ConfirmInProbation:
                create_workflow_button(frm, action)
                break


            case WorkflowActionsMetadata.Cancel:
                create_workflow_button(frm, action)
                break
        }
    })
}


async function get_list_workflow_actions(frm) {
    let list_action_response = await Promise.resolve(
        frappe.call({
            method: APIPath.GetListWorkflowAction,
            args: {
                current_workflow_state: frm.doc.workflow_state
            }
        }),
    )
    return list_action_response.message
}


async function compose_mail(frm, doc, recipients, message, email_template = '', send_callback, attachment = null, communication_doc = null, reply_all = false,) {
    let mail_message = ''

    switch (email_template) {
        case EmailTemplate.OfferLetter:
            mail_message = await Promise.resolve(
                frappe.call({
                    method: APIPath.GenerateOfferLetterTemplate,
                    args: {
                        employee: frm.doc.name,
                        contract_id: primary_enable_probation_contract.name
                    }
                }),
            )
            break
        case EmailTemplate.Agenda:
            mail_message = await Promise.resolve(
                frappe.call({
                    method: APIPath.GenerateOnboardingAgendaTemplate,
                    args: {
                        employee: frm.doc.name
                    }
                }),
            )
    }

    console.log('compose_mail message')
    console.log(mail_message)

    const args = {
        doc: doc,
        frm: frm,
        recipients: recipients,
        is_a_reply: Boolean(communication_doc),
        title: communication_doc ? __("Reply") : null,
        last_email: communication_doc,
        subject: communication_doc && communication_doc.subject,
        now: 1,
        success: send_callback
    };

    if (communication_doc && reply_all) {
        args.cc = communication_doc.cc;
        args.bcc = communication_doc.bcc;
    }

    if (email_template) {
        args.message = mail_message.message
    } else if (message) {
        args.message = message
    } else {
        args.message = ''
    }

    if (attachment && Array.isArray(attachment)) {
        args.attachments = attachment
    }

    args.attached_files_folder = `Home/Employee/${frm.doc.name}`

    new CustomCommunicationComposer(args);
    // new frappe.views.CommunicationComposer(args)
}


/// SEND OFFER LOGIC ///

function change_mandatory_addresses(frm, is_mandatory) {
    frm.set_df_property('permanent_address_country', 'reqd', is_mandatory)
    frm.set_df_property('permanent_address_province', 'reqd', is_mandatory)
    frm.set_df_property('permanent_address_district', 'reqd', is_mandatory)
    frm.set_df_property('permanent_address_ward', 'reqd', is_mandatory)
    frm.set_df_property('permanent_address_street', 'reqd', is_mandatory)
}

function change_mandatory_personal_document(frm, is_mandatory) {
    frm.set_df_property('personal_doc', 'reqd', is_mandatory)
}

function has_personal_id(frm) {
    let personal_documents = frm.doc.personal_doc
    console.log('personal_documents')
    console.log(personal_documents)
    if (personal_documents) {
        for (let i = 0; i < personal_documents.length; i++) {
            if ([PersonalDocumentTypeMetadata.CCCD, PersonalDocumentTypeMetadata.CMND, PersonalDocumentTypeMetadata.Passport].includes(personal_documents[i].type)) {
                if (personal_documents[i].number && personal_documents[i].attached_file) {
                    return true
                }
            }
        }
    }
    return false
}

function has_work_permit(frm) {
    let personal_documents = frm.doc.personal_doc
    let has_valid_visa = false
    let has_valid_work_permit = false
    if (personal_documents) {
        for (let i = 0; i < personal_documents.length; i++) {
            if (personal_documents[i].type == PersonalDocumentTypeMetadata.Visa) {
                if (personal_documents[i].expiration_date > frappe.datetime.get_today() && personal_documents[i].attached_file) {
                    has_valid_visa = true
                }
            } else if (personal_documents[i].type == PersonalDocumentTypeMetadata.WorkPermit) {
                if (personal_documents[i].expiration_date > frappe.datetime.get_today() && personal_documents[i].attached_file) {
                    has_valid_work_permit = true
                }
            }
        }
    }

    return (has_valid_visa && has_valid_work_permit)
}


function has_permanent_address(frm) {
    return (frm.doc.permanent_address_province && frm.doc.permanent_address_district && frm.doc.permanent_address_ward && frm.doc.permanent_address_street)
}

function has_current_address(frm) {
    return (frm.doc.current_address_province && frm.doc.current_address_district && frm.doc.current_address_ward && frm.doc.current_address_street)
}

function can_create_job_offer(frm) {
    // if (has_permanent_address(frm) && has_current_address(frm) && ((frm.doc.nationality == "Vietnam" && has_personal_id(frm)) || has_work_permit(frm))){
    if ((frm.doc.nationality == "Vietnam" && has_personal_id(frm)) || has_work_permit(frm)) {
        return true
    }
    return false
}


function change_same_as_current_address_checkbox(frm, address_component_1, address_component_2) {
    if (address_component_1 != address_component_2) {
        frm.set_value('same_as_current_address', 0)
    }
}

function set_year_code(frm) {
    if (!frm.doc.year_code) {
        let year_code = frappe.datetime.get_today().substring(2, 4)
        frm.set_value('year_code', year_code)
    }
}

/// CANDIDATE SOUCE ///

function set_display_source_internal_referral(frm) {
    console.log('set_display_source_internal_referral')
    console.log(frm.doc.type_metadata_item)
    console.log(frm.doc.source_type)
    if (frm.doc.type_metadata_item == ReferralMetadata.InternalReferral && frm.doc.source_type == CandidateSourceMetadata.Referral) {
        frm.toggle_display('source_internal_referrer', 1)
    } else {
        if (frm.doc.source_internal_referrer) {
            console.log('set source_internal_referrer to null')
            frm.set_value('source_internal_referrer', null)
        }
        frm.toggle_display('source_internal_referrer', 0)
    }
}

async function get_query_source_type_metadata(frm) {
    let list_metadata = await Promise.resolve(
        frappe.db.get_list(
            'Source Type Metadata',
            {
                fields: ['metadata'],
                filters: {
                    type: frm.doc.source_type || "",
                }
            }
        )
    )

    let res = []
    if (list_metadata) {
        for (let i = 0; i < list_metadata.length; i++) {
            res[i] = list_metadata[i].metadata
        }
    }
    return res

}

async function set_query_source_type_metadata(frm) {
    let list_metadata = await get_query_source_type_metadata(frm)
    if (list_metadata.length > 0) {
        list_metadata.unshift('\n')
        frm.toggle_display('type_metadata_item', 1)
        frappe.meta.get_docfield('Employee', 'type_metadata_item', frm.doc.name).options = list_metadata
        frm.refresh_field('type_metadata_item')
    } else {
        if (frm.doc.source_type === undefined || frm.doc.source_type === '' || frm.doc.source_type === null) {
            if (frm.doc.type_metadata_item) {
                console.log('set type_metadata_item to null')
                frm.set_value('type_metadata_item', null)
            }
        }
        frm.toggle_display('type_metadata_item', 0)
    }

}

frappe.ui.form.on('Employee', {

    refresh: async function (frm) {
        /// HANDLE INIT DATA FOR ADDRESSES
        handle_province_quick_entry(frm, "permanent_address_province", frm.doc.permanent_address_country)
        handle_district_quick_entry(frm, "permanent_address_district", frm.doc.permanent_address_province)
        handle_ward_quick_entry(frm, "permanent_address_ward", frm.doc.permanent_address_ward)
        handle_province_quick_entry(frm, "current_address_province", frm.doc.current_address_country)
        handle_district_quick_entry(frm, "current_address_district", frm.doc.current_address_province)
        handle_ward_quick_entry(frm, "current_address_ward", frm.doc.current_address_ward)

        set_query_for_province(frm, frm.doc.permanent_address_country, 'permanent_address_province')
        set_query_for_province(frm, frm.doc.current_address_country, 'current_address_province')

        if (await has_valid_probation_contract(frm)) {
            // change_mandatory_addresses(frm, 1)
            change_mandatory_personal_document(frm, 1)
            console.log('refresh primary_enable_probation_contract')
            console.log(primary_enable_probation_contract)
            if (frm.doc.workflow_state == EmployeeWorkflowState.InterviewPassed && primary_enable_probation_contract) {
                if (primary_enable_probation_contract.workflow_state == ContractApprovalWorkflowState.Approved) {
                    has_enable_primary_approved_contract = true
                    change_workflow_state(frm, EmployeeWorkflowState.OnboardOffering)
                }
            }
        }

        if (!(frm.doc.work_email)) {
            if (!(frm.doc.private_email)) {
                frm.set_df_property('private_email', 'reqd', 1)
            }
        }

        console.log('back_from_contract')
        console.log(back_from_contract)
        if (back_from_contract) {
            back_from_contract = false
            if (await has_valid_probation_contract(frm) && frm.doc.workflow_state == EmployeeWorkflowState.InterviewPassed) {
                if (primary_enable_probation_contract) {
                    if (primary_enable_probation_contract.workflow_state == ContractApprovalWorkflowState.Approved) {
                        has_enable_primary_approved_contract = true
                        change_workflow_state(frm, EmployeeWorkflowState.OnboardOffering)

                    }
                }
                update_contracts_info(frm)
                frm.reload_doc()
            }
        }

        list_actions = await get_list_workflow_actions(frm)
        console.log('list_actions refresh')
        console.log(list_actions)
        console.log('current workflow state')
        console.log(frm.doc.workflow_state)
        create_list_action_buttons(frm)
    },

    onload: async function (frm) {
        current_intro_message = null

        console.log('onload')
        console.log(current_intro_message)
        console.log(frm.doc.workflow_state)
        console.log(frappe.session.user)

        /// temporarily hide this tab for further development
        frm.toggle_display('remove_termination_btn', false)
        frm.toggle_display('termination_wizard', false)
        set_year_code(frm)
        // await set_display_source_internal_referrer(frm)

        await set_query_source_type_metadata(frm)
        set_display_source_internal_referral(frm)
    },

    source_type: async function (frm) {
        // await set_display_source_internal_referrer(frm)
        await set_query_source_type_metadata(frm)
        set_display_source_internal_referral(frm)

    },

    type_metadata_item: function (frm) {
        set_display_source_internal_referral(frm)
    },

    first_name: function (frm) {
        update_full_name(frm)
        update_employee_status(frm)
    },

    middle_name: function (frm) {
        update_full_name(frm)
        update_employee_status(frm)
    },

    last_name: function (frm) {
        update_full_name(frm)
        update_employee_status(frm)
    },

    private_email: function (frm) {
        update_employee_status(frm)
    },

    profile_picture: function (frm) {
        if (frm.doc.profile_picture) {
            frappe.utils.get_file_link(
                frm.doc.profile_picture
            )
        }
    },

    /// Handle current address
    current_address_country: function (frm) {
        let current_country = frm.doc.current_address_country
        change_same_as_current_address_checkbox(frm, current_country, frm.doc.permanent_address_country)

        if (current_country) {
            frm.set_df_property('current_address_province', 'read_only', 0)
            if (current_country != selected_current_country) {
                set_query_for_province(frm, current_country, 'current_address_province')
                frm.set_value('current_address_province', null)
            }
        } else {
            frm.set_value('current_address_province', null)
            frm.set_df_property('current_address_province', 'read_only', 1)
        }
        selected_current_country = current_country
    },

    current_address_province: function (frm) {
        let current_province = frm.doc.current_address_province
        change_same_as_current_address_checkbox(frm, current_province, frm.doc.permanent_address_province)

        if (current_province) {
            frm.set_df_property('current_address_district', 'read_only', 0)
            if (current_province != selected_current_province) {
                set_query_for_district(frm, current_province, 'current_address_district')
                frm.set_value('current_address_district', null)
            }
        } else {
            frm.set_value('current_address_district', null)
            frm.set_df_property('current_address_district', 'read_only', 1)
        }
        selected_current_province = current_province
    },

    current_address_district: function (frm) {
        let current_district = frm.doc.current_address_district
        change_same_as_current_address_checkbox(frm, current_district, frm.doc.permanent_address_district)

        if (current_district) {
            frm.set_df_property('current_address_ward', 'read_only', 0)
            if (current_district != selected_current_district) {
                set_query_for_ward(frm, current_district, 'current_address_ward')
                frm.set_value('current_address_ward', null)
            }
        } else {
            frm.set_value('current_address_ward', null)
            frm.set_df_property('current_address_ward', 'read_only', 1)
        }
        selected_current_district = current_district
    },

    current_address_ward: function (frm) {
        change_same_as_current_address_checkbox(frm, frm.doc.current_address_ward, frm.doc.permamnent_address_ward)
    },

    current_address_street: function (frm) {
        change_same_as_current_address_checkbox(frm, frm.doc.current_address_street, frm.doc.permanent_address_street)
    },

    same_as_current_address: function (frm) {
        if (frm.doc.same_as_current_address) {
            if (frm.doc.permanent_address_country == frm.doc.current_address_country) {
                if (frm.doc.permanent_address_province == frm.doc.current_address_province) {
                    if (frm.doc.permanent_address_district == frm.doc.current_address_district) {
                        frm.set_value('permanent_address_ward', frm.doc.current_address_ward)
                    } else {
                        frm.set_value('permanent_address_district', frm.doc.current_address_district)
                    }
                } else {
                    frm.set_value('permanent_address_province', frm.doc.current_address_province)
                }
            } else {
                frm.set_value('permanent_address_country', frm.doc.current_address_country)
            }
            frm.set_value('permanent_address_street', frm.doc.current_address_street)
        }
    },

    /// Handle permanent address
    permanent_address_country: function (frm) {
        let permanent_country = frm.doc.permanent_address_country
        change_same_as_current_address_checkbox(frm, permanent_country, frm.doc.current_address_country)

        if (permanent_country) {
            frm.set_df_property('permanent_address_province', 'read_only', 0)
            if (permanent_country != selected_permanent_country) {
                frm.set_value('permanent_address_province', null)
                set_query_for_province(frm, permanent_country, 'permanent_address_province')
            }
            if (frm.doc.same_as_current_address) {
                frm.set_value('permanent_address_province', frm.doc.current_address_province)
            }
        } else {
            frm.set_value('permanent_address_province', null)
            frm.set_df_property('permanent_address_province', 'read_only', 1)
        }

        selected_permanent_country = permanent_country
    },

    permanent_address_province: function (frm) {
        let permanent_province = frm.doc.permanent_address_province
        change_same_as_current_address_checkbox(frm, permanent_province, frm.doc.current_address_province)

        if (permanent_province) {
            frm.set_df_property('permanent_address_district', 'read_only', 0)
            if (permanent_province != selected_permanent_province) {
                frm.set_value('permanent_address_district', null)
                set_query_for_district(frm, permanent_province, 'permanent_address_district')
            }
            if (frm.doc.same_as_current_address) {
                frm.set_value('permanent_address_district', frm.doc.current_address_district)
            }
        } else {
            frm.set_value('permanent_address_district', null)
            frm.set_df_property('permanent_address_district', 'read_only', 1)
        }

        selected_permanent_province = permanent_province
    },

    permanent_address_district: function (frm) {
        let permanent_district = frm.doc.permanent_address_district
        change_same_as_current_address_checkbox(frm, permanent_district, frm.doc.current_address_district)

        if (permanent_district) {
            frm.set_df_property('permanent_address_ward', 'read_only', 0)
            if (permanent_district != selected_permanent_district) {
                frm.set_value('permanent_address_ward', null)
                set_query_for_ward(frm, permanent_district, 'permanent_address_ward')
            }
            if (frm.doc.same_as_current_address) {
                frm.set_value('permanent_address_ward', frm.doc.current_address_ward)
            }
        } else {
            frm.set_value('permanent_address_ward', null)
            frm.set_df_property('permanent_address_ward', 'read_only', 1)
        }

        selected_permanent_district = permanent_district
    },

    permanent_address_ward: function (frm) {
        change_same_as_current_address_checkbox(frm, frm.doc.permanent_address_ward, frm.doc.current_address_ward)
    },

    permanent_address_street: function (frm) {
        change_same_as_current_address_checkbox(frm, frm.doc.permanent_address_street, frm.doc.current_address_street)
    },

    // line_manager: function(frm){
    //     frappe.form.link_formatters['Line Manager'] = function(value, doc){
    //         console.log('link formatter employee called')
    //         if (doc && doc.full_name && doc.full_name !== value){
    //             return value + ": " + doc.full_name;
    //             // return value.concat(': ', doc.full_name)
    //         }else{
    //             return value + ": ---";
    //             // return value
    //         }
    //     }
    // },

    /// Handle termination wizard onTap
    termination_wizard: function (frm) {

        // if (has_primary_contract) {
        //     let dialog = new frappe.ui.Dialog({

        //         title: 'Termination',
        //         fields: [
        //             {
        //                 'label': 'Last working date',
        //                 'fieldname': 'last_working_date',
        //                 'fieldtype': 'Date',
        //                 "default": `${primary_contract.end_date}`,
        //                 'reqd': 1,
        //             },
        //             {
        //                 'label': "Business Account Deactivation Day",
        //                 'fieldname': "business_account_deactivation_date",
        //                 'in_list_view': 1,
        //                 'options': `\n${DeactivationStateMetadata.Immediately}\n${DeactivationStateMetadata.AfterDate}`,
        //                 'fieldtype': "Select",
        //             },
        //             {
        //                 'label': 'Deactivation Day',
        //                 'fieldname': 'after_date',
        //                 'fieldtype': 'Date',
        //                 "default": `${primary_contract.end_date}`,
        //                 "depends_on": "eval: doc.business_account_deactivation_date == 'After Date'"

        //             },
        //             {
        //                 'label': "Termination Reason",
        //                 'fieldname': "termination_reason",
        //                 'in_list_view': 1,
        //                 'options': "Fired\nResigned\nRetired\nChange Company",
        //                 'fieldtype': "Select"
        //             },
        //             {
        //                 'label': "Detailed Reason",
        //                 'fieldname': "detailed_reason",
        //                 'fieldtype': "Data"
        //             }
        //         ],
        //         primary_action_label: 'Submit',
        //         primary_action(values) {
        //             frm.set_value('terminate_last_working_date', values.last_working_date)
        //             frm.set_value(
        //                 'business_acc_deact_date',
        //                 values.business_account_deactivation_date == DeactivationStateMetadata.Immediately
        //                     ? frappe.datetime.get_today()
        //                     : values.after_date
        //             )
        //             frm.set_value('termination_reason', values.termination_reason)
        //             frm.set_value('detailed_reason', values.detailed_reason)

        //             if (frappe.datetime.get_today() > values.terminate_last_working_date) {
        //                 frm.set_value('employee_contract_status', employee_contract_status_metadata.Terminated)
        //             } else {
        //                 frm.set_value('employee_contract_status', employee_contract_status_metadata.UnderTerminationProcess)
        //                 update_cancel_termination_process_button(frm)
        //             }

        //             frm.save()
        //             dialog.hide();
        //         }
        //     })

        //     dialog.show()
        // } else {
        //     frappe.throw(__('This employee has no contract.'))
        // }

    },

    remove_termination_btn: function (frm) {
        remove_termination_process(frm)
    },

    nationality: function (frm) {
        update_full_name(frm)
    },

    linked_user: function (frm) {
        console.log(frm.doc.linked_user)
    },

    work_location: function (frm) {
        console.log(frm.doc.work_location)
        if (frm.doc.work_location && frm.doc.work_location.length == 1) {
            frappe.db.get_value('Core Address', frm.doc.work_location[0].work_location_address, ['country', 'province', 'district', 'ward', 'street']).then(r => {
                console.log('work location')
                console.log(r.message)
            })
        }
    },

    personal_doc_on_form_rendered: function (frm) {
        console.log('personal_doc_on_form_rendered')
        console.log(frm.doc.personal_doc.length)
    }
});

frappe.ui.form.on('Documents', {
    file: function (frm, cdt, cdn) {
        let row = frappe.get_doc(cdt, cdn)
        row.uploaded_date = frappe.datetime.get_today()
        row.uploaded_by = frappe.session.user
        if (row.file) {
            console.log("file is not empty")
        } else {
            row.uploaded_date = null
        }
    },
})

frappe.ui.form.on('Personal Document', {
    primary: function (frm, cdt, cdn) {
        let current_row = locals[cdt][cdn]
        let child = locals[cdt]
        console.log(current_row)
        console.log(child)

        if (current_row.primary) {
            for (var row_name in child) {
                if (row_name != current_row.name) {
                    child[row_name].primary = false
                    cur_frm.refresh_field("personal_doc")
                }
            }
        }
    },

    type: function (frm, cdt, cdn) {
        console.log(frm.fields_dict)
        let is_cmnd_cccd = locals[cdt][cdn].type === PersonalDocumentTypeMetadata.CCCD || locals[cdt][cdn].type === PersonalDocumentTypeMetadata.CMND
        frm.fields_dict.personal_doc.grid.toggle_reqd("issued_by", is_cmnd_cccd)
        frm.fields_dict.personal_doc.grid.toggle_reqd("number", is_cmnd_cccd)

        if (frm.fields_dict.personal_doc.grid.data.length === 1 && is_cmnd_cccd) {
            locals[cdt][cdn].primary = true
        }
    },
})

frappe.ui.form.on('Contract Information', {
    view_file: function (frm, cdt, cdn) {
        let current_row = locals[cdt][cdn]
        let d = new frappe.ui.Dialog({
            title: current_row.code,
            fields: [
                {
                    fieldname: 'pdf_view',
                    fieldtype: 'HTML'
                }
            ],
        });
        /// time is added as a param to always load new file
        d.set_value('pdf_view', `<object data="${current_row.pdf_link}#toolbar=0" width="100%" height="700"></object>`)
        d.show()
        $(d.$wrapper.find('.modal-dialog')).addClass('modal-lg')
    }
})

