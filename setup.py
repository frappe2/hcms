from setuptools import setup, find_packages

with open("requirements.txt") as f:
	install_requires = f.read().strip().split("\n")

# get version from __version__ variable in hcms/__init__.py
from hcms import __version__ as version

setup(
	name="hcms",
	version=version,
	description="Internal app",
	author="masterisehomes",
	author_email="developer@masterisehomes.com",
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
