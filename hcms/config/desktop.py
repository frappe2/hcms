from frappe import _

def get_data():
	return [
		{
			"module_name": "Human Capital Management System",
			"type": "module",
			"label": _("Human Capital Management System")
		}
	]
