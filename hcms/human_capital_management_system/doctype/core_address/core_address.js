// Copyright (c) 2022, masterisehomes and contributors
// For license information, please see license.txt

var selected_country;
var selected_province;
var selected_district;

function set_query_for_province(frm, current_country){
	frm.set_query('province', () => {
		return {
			filters: {
				country: current_country
			}
		}
	})
}

function set_query_for_district(frm, current_province){
	frm.set_query('district', () => {
		return {
			filters: {
				province: current_province
			}
		}
	})
}

function set_query_for_ward(frm, current_district){
	frm.set_query('ward', () => {
		return {
			filters: {
				district: current_district
			}
		}
	})
}

async function set_value_full_address(frm){
	let ward = ''
	let district = ''
	let province = ''
	if (frm.doc.ward){
		ward = `${await get_name_ward(frm)}, `
	}
	if (frm.doc.district){
		district = `${await get_name_district(frm)}, `
	}
	if (frm.doc.province){
		province = `${await get_name_province(frm)}, `
	}
	let full_address = `${frm.doc.street}, ${ward}${district}${province}${frm.doc.country}`
	frm.set_value('full_address', full_address)
}

async function get_name_province(frm){
	let result = await Promise.resolve(frappe.db.get_value('Province', frm.doc.province, 'province'))
	return result.message.province
}

async function get_name_district(frm){
	let result = await Promise.resolve(frappe.db.get_value('District', frm.doc.district, 'district'))
	return result.message.district
}

async function get_name_ward(frm){
	let result = await Promise.resolve(frappe.db.get_value('Ward', frm.doc.ward, 'ward'))
	return result.message.ward
}

frappe.ui.form.on('Core Address', {

	onload: function(frm){
		let selected_country = frm.doc.country
		let selected_province = frm.doc.province
		let selected_district = frm.doc.district

		if (!selected_country){
			frm.set_value('country', null)
		}else{
			set_query_for_province(frm, selected_country)
		}
		
		if (!selected_province){
			frm.set_value('province', null)
		}else{
			set_query_for_district(frm, selected_province)
		}
		
		if (!selected_district){
			frm.set_value('district', null)
		}else{
			set_query_for_ward(frm, selected_district)
		}

	},

	refresh: function(frm){

		/// Province quick entry
		frm.get_docfield("province").get_route_options_for_new_doc = function(field){
			return {
				"country": frm.doc.country
			}
		}

		/// District quick entry
		frm.get_docfield("district").get_route_options_for_new_doc = function(field){
			return {
				"province": frm.doc.province
			}
		}

		/// Ward quick entry
		frm.get_docfield("ward").get_route_options_for_new_doc = function(field){
			return {
				"district": frm.doc.district
			}
		}
	},


	country: function(frm){
		let current_country = frm.doc.country

		if (current_country){

			frm.set_df_property('province', 'read_only', 0)

			if (current_country != selected_country){
				set_query_for_province(frm, current_country)
				frm.set_value('province', null)
			}
		}else{
			frm.set_value('province', null)
			frm.set_df_property('province', 'read_only', 1)
		}

		selected_country = current_country
		set_value_full_address(frm)
	},


	province: function(frm){
		let current_province = frm.doc.province

		

		if (current_province){

			frm.set_df_property('district', 'read_only', 0)

			if (current_province != selected_province){
				set_query_for_district(frm, current_province)
				frm.set_value('district', null)
			}
		}else{
			frm.set_value('district', null)
			frm.set_df_property('district', 'read_only', 1)
		}

		selected_province = current_province
		set_value_full_address(frm)
	},

	district: function(frm){
		let current_district = frm.doc.district

		if (current_district){
			frm.set_df_property('ward', 'read_only', 0)
			if (current_district != selected_district){
				set_query_for_ward(frm, current_district)
				frm.set_value('ward', null)
			}
		}else{
			frm.set_value('ward', null)
			frm.set_df_property('ward', 'read_only', 1)
		}

		selected_district = current_district
		set_value_full_address(frm)
	},

	ward: function(frm){
		set_value_full_address(frm)
	},

	street: function(frm){
		set_value_full_address(frm)
	}
});

