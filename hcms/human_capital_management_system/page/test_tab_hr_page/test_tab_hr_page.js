frappe.pages['test-tab-hr-page'].on_page_load = function(wrapper) {
	const test_tab_hr_page = new TestTabHRPage(wrapper)
}

class TestTabHRPage {
	constructor(wrapper){
		this.page = frappe.ui.make_app_page({
			parent: wrapper,
			title: 'Tab 1 HR Page',
			single_column: true
		});

		this.wrapper = wrapper;

		this.make()
	}

	async make(){
		let full_name = this.page.add_field({
			label: 'Name',
			fieldtype: 'Data',
			fieldname: 'name',
			change() {
				console.log(full_name.get_value());
			}
		})

		let age = this.page.add_field({
			label: 'Age',
			fieldtype: 'Data',
			fieldname: 'age',
			change() {
				console.log(age.get_value());
			}
		})

		let data_query_report_raw = await Promise.resolve(
			frappe.call({
				method: "frappe.desk.query_report.run",
				type: "GET",
				args: {
					report_name: 'test-report-hr',
					// filters: filters,
					// is_tree: this.report_settings.tree,
					// parent_field: this.report_settings.parent_field,
				},
			})
		)

		let data_query_report = data_query_report_raw.message

		let datatable_options = {
			columns: this.prepare_columns(data_query_report.columns),
			data: this.prepare_data(data_query_report.result),
			inlineFilters: true,
			language: frappe.boot.lang,
			translations: frappe.utils.datatable.get_translations(),
			layout: "fixed",
			cellHeight: 33,
			direction: frappe.utils.is_rtl() ? "rtl" : "ltr",
			hooks: {
				columnTotal: frappe.utils.report_column_total,
			},
		};

		this.$report = $('<div class="report-wrapper">').appendTo(this.page.main);
		this.$report.show()
		this.datatable = new DataTable(this.$report[0], datatable_options);

	}

	prepare_columns(columns) {
		return columns.map((column) => {
			column = frappe.report_utils.prepare_field_from_column(column);

			const format_cell = (value, row, column, data) => {
				if (column.isHeader && !data && this.data) {
					// totalRow doesn't have a data object
					// proxy it using the first data object
					// applied to Float, Currency fields, needed only for currency formatting.
					// make first data column have value 'Total'
					let index = 1;

					if (column.colIndex === index && !value) {
						value = "Total";
						column = { fieldtype: "Data" }; // avoid type issues for value if Date column
					}
				}
				return frappe.format(
					value,
					column,
					{ for_print: false, always_show_decimals: true },
					data
				);
			};

			let compareFn = null;
			if (column.fieldtype === "Date") {
				compareFn = (cell, keyword) => {
					if (!cell.content) return null;
					if (keyword.length !== "YYYY-MM-DD".length) return null;

					const keywordValue = frappe.datetime.user_to_obj(keyword);
					const cellValue = frappe.datetime.str_to_obj(cell.content);
					return [+cellValue, +keywordValue];
				};
			}

			return Object.assign(column, {
				id: column.fieldname,
				name: __(column.label, null, `Report`), // context has to match context in   get_messages_from_report in translate.py
				width: parseInt(column.width) || null,
				editable: false,
				compareValue: compareFn,
				format: (value, row, column, data) => {
					return format_cell(value, row, column, data);
				},
			});
		});
	}

	prepare_data(data) {
		return data.map((row) => {
			let row_obj = {};
			if (Array.isArray(row)) {
				this.columns.forEach((column, i) => {
					row_obj[column.id] = row[i];
				});

				return row_obj;
			}
			return row;
		});
	}
}