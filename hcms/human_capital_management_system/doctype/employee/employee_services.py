# Copyright (c) 2022, MasteriseHomes and contributors
# For license information, please see license.txt

from hcms_core.services.api_response import APIResponse
from hcms_core.services.api_services import APIService


import requests
from requests import Response
import json
from dataclasses import fields
from email import message
from urllib import request
from venv import create
import frappe
from frappe.model.document import Document
import string
import random
import jinja2
from hcms_core.utils.utils import CoreUtils
# from hcms_contract.hcms_contract.doctype.contract.contract import Contract
import codecs
import os
# from hcms_contract.hcms_contract.doctype.contract.contract import (
#     ContractStatusMetadata,
#     ContractTypeMetadata,
#     ContractTermMetadata,
# )

# from hcms_contract.tasks import update_employee_contract_status
import requests
import math
from frappe.utils import now, getdate, get_site_name
import re
from frappe.permissions import (
    add_user_permission,
    has_permission,
)
from hcms.config.constant import *
from hcms_contract.config.constant import *
import jwt
from datetime import datetime
# import hcms_core.utils as core
# from hcms_core import core
# import hcms_core.hcms_core as core

import hcms_core.utils.constants as core
import hcms_core.services.api_exception as core_exception
from hcms.human_capital_management_system.doctype.employee.employee import Employee
from hcms_contract.hcms_contract.doctype.contract.contract import Contract
from frappe.utils.jinja import get_jenv


@frappe.whitelist()
def get_list_workflow_actions(current_workflow_state):
    emp_workflow = frappe.get_doc(
        core.CoreConstants.DoctypeName.Workflow, 'Employee Workflow')

    list_user_role = frappe.get_roles(frappe.session.user)
    list_actions = []

    for transition in emp_workflow.transitions:
        if ((transition.allowed in list_user_role) and (current_workflow_state == transition.state)):
            list_actions.append(
                {
                    'action': transition.action,
                    'next_state': transition.next_state
                }
            )

    return list_actions


def _get_list_transition(employee):
    emp = frappe.get_doc(core.CoreConstants.DoctypeName.Employee, employee)
    print('change_workflow_state current workflow state')
    print(emp.workflow_state)
    emp_workflow = frappe.get_doc(
        core.CoreConstants.DoctypeName.Workflow, 'Employee Workflow')

    return emp_workflow.transitions, emp_workflow.states, emp.workflow_state


def update_employee_status(employee, current_state, list_state):
    employee_status = None

    emp = frappe.get_doc(core.CoreConstants.DoctypeName.Employee, employee)

    match (current_state):
        case EmployeeWorkflowState.InterviewCVAssessment.value:
            if (emp.last_name and emp.middle_name and emp.first_name and emp.private_email):
                employee_status = EmployeeStatusMetadata.Applicant.value

        case EmployeeWorkflowState.InterviewUnderInterview.value:
            employee_status = EmployeeStatusMetadata.Candidate.value

        case EmployeeWorkflowState.InterviewPassed.value:
            employee_status = EmployeeStatusMetadata.Preboarding.value

        case EmployeeWorkflowState.InterviewRejected.value:
            employee_status = EmployeeStatusMetadata.ApplicantJunk.value

        case EmployeeWorkflowState.OnboardOffering.value:
            employee_status = EmployeeStatusMetadata.Preboarding.value

        case EmployeeWorkflowState.OnboardOfferAccepting.value:
            employee_status = EmployeeStatusMetadata.Preboarding.value

        case EmployeeWorkflowState.OnboardOfferAccepted.value:
            employee_status = EmployeeStatusMetadata.Preboarding.value

        case EmployeeWorkflowState.OnboardOfferDenied.value:
            employee_status = EmployeeStatusMetadata.CandidateJunk.value

        case EmployeeWorkflowState.OnboardAbsence.value:
            employee_status = EmployeeStatusMetadata.Preboarding.value

        case EmployeeWorkflowState.OnboardShowingUp.value:
            employee_status = EmployeeStatusMetadata.Onboarding.value

        case EmployeeWorkflowState.OnboardInfoFilled.value:
            employee_status = EmployeeStatusMetadata.Onboarding.value

        case EmployeeWorkflowState.OnboardContractSigned.value:
            employee_status = EmployeeStatusMetadata.Onboarding.value

        case EmployeeWorkflowState.OnboardInProbation.value:
            employee_status = EmployeeStatusMetadata.InProbation.value

        case EmployeeWorkflowState.OnboardCancelled.value:
            employee_status = EmployeeStatusMetadata.CandidateJunk.value

    if (employee_status):
        frappe.db.set_value(core.CoreConstants.DoctypeName.Employee, employee, "status", employee_status)


def _get_dict_state(list_state):
    dict_state = {}
    for i in range(len(list_state)):
        if (list_state[i].parent == EMPLOYEE_WORKFLOW):
            dict_state[list_state[i].state] = {
                "key": list_state[i].update_field,
                "value": list_state[i].update_value,
                "allow_edit": list_state[i].allow_edit,
            }
    return dict_state


def _get_list_email_hiring_manager() -> list:
    list_email = frappe.db.sql(
        """
            SELECT parent
            FROM `tabHas Role` AS tHR
            INNER JOIN (
                SELECT tU.name
                FROM tabEmployee AS tE
                INNER JOIN tabUser tU
                ON tE.work_email = tU.email
                WHERE tE.status = 'Active') AS tEmail
            ON tHR.parent = tEmail.name
            WHERE tHR.role = 'Hiring Manager'
        """,
        as_dict=True
    )
    print('_get_list_email_hiring_manager')
    print(list_email)
    return list_email


def _notify_hiring_manager(message: str):
    print('_notify_hiring_manager _get_list_email_hiring_manager')
    list_email = _get_list_email_hiring_manager()

    subject: str = 'Onboarding Notification'
    for email in list_email:
        frappe.sendmail(
            recipients=email['parent'],
            subject=subject,
            message=message,
            now=True,
        )


def _get_list_employee_code() -> list:
    list_emp = frappe.db.sql(
        """
            SELECT
                RIGHT(code, 4) AS code
            FROM tabEmployee
            ORDER BY code DESC
            LIMIT 1
        """,
        as_dict=True,
    )
    return list_emp


def _generate_employee_code(employee: str):
    contract = frappe.db.get_value(
        'Contract',
        {
            "employee": employee,
            "type": ContractTypeMetadata.Contract.value,
            "term": ContractTermMetadata.Probation.value,
            "status": ContractStatusMetadata.Running.value,
            "enable": 1,
            "is_primary": 1,
        },
        ['start_date', 'company'],
        as_dict=1,
    )
    company_code = contract['company']
    year_code = str(contract['start_date'])[2:4]

    list_emp_code = _get_list_employee_code()
    employee_code = ''
    employee_id = '0000'

    if (list_emp_code):
        if (list_emp_code[0]['code'] is not None):
            employee_id = str(int(list_emp_code[0]['code']) + 1)
            employee_id = employee_id.rjust(4, '0')

    employee_code = f"{company_code}-{year_code}-{employee_id}"

    frappe.db.set_value('Employee', employee, "code", employee_code)


@frappe.whitelist()
def change_workflow_state(employee, new_state, should_notify_manager="0", notify_message=None):
    list_user_role = frappe.get_roles(frappe.session.user)
    list_transition, list_state, current_workflow_state = _get_list_transition(
        employee=employee)
    print('list state')
    print(list_state)
    is_new_state_valid = False

    # dict_state = _get_dict_state(list_state=list_state)

    for transition in list_transition:
        if ((transition.allowed in list_user_role) and (current_workflow_state == transition.state) and (new_state == transition.next_state)):
            is_new_state_valid = True

        if is_new_state_valid:
            frappe.db.set_value(
                core.CoreConstants.DoctypeName.Employee, employee, "workflow_state", new_state)
            update_employee_status(
                employee=employee, current_state=new_state, list_state=list_state)
            if (new_state == EmployeeWorkflowState.OnboardShowingUp.value):
                _generate_employee_code(employee=employee)

            if (should_notify_manager == "1"):
                _notify_hiring_manager(message=notify_message)
            return True

    return False


@frappe.whitelist()
def remove_termination(employee):
    pass
    # return update_employee_contract_status(employee, getdate())


@frappe.whitelist()
def create_ldap_user_account(employee):
    try:
        emp = frappe.get_doc(core.CoreConstants.DoctypeName.Employee, employee)
        if (emp.work_email):
            return True
        else:
            username = _create_valid_user_account(
                first_name=emp.first_name, last_name=emp.last_name, middle_name=emp.middle_name)
            business_email = username + '@yopmail.com'

            user = frappe.new_doc(core.CoreConstants.DoctypeName.User)
            user.update(
                {
                    "email": business_email,
                    "enabled": 1,
                    "first_name": emp.first_name,
                    "middle_name": emp.middle_name,
                    "last_name": emp.last_name,
                    "new_password": username
                }
            )
            user.insert()
            user.append('roles', {
                "doctype": core.CoreConstants.DoctypeName.HasRole,
                "role": "Candidate"
            })
            user.save(ignore_permissions=True)
            frappe.db.set_value(
                core.CoreConstants.DoctypeName.Employee, employee, "work_email", business_email)
            add_user_permission_for_employee(employee)
            return True
    except:
        return False


# Allow this employee to access only to its corresponding user
def add_user_permission_for_employee(employee):

    emp = frappe.get_doc(core.CoreConstants.DoctypeName.Employee, employee)

    # have no work_email => have no user for this employee
    if (not emp.work_email):
        return

    if (not has_permission(core.CoreConstants.DoctypeName.UserPermission, ptype="write", raise_exception=False)):
        return

    employee_user_permission_exists = frappe.db.exists(
        core.CoreConstants.DoctypeName.UserPermission, {
            "allow": core.CoreConstants.DoctypeName.Employee, "for_value": emp.name, "user": emp.work_email}
    )

    if (employee_user_permission_exists):
        return

    add_user_permission(
        core.CoreConstants.DoctypeName.Employee, emp.name, emp.work_email)


def get_working_location_address(address):
    core_address = address.core_address
    street = core_address.street
    ward = core_address.ward
    district = core_address.district
    province = core_address.province
    return street + ', ' + ward + ', ' + district + ', ' + province


def has_workflow_action_permission(action: str, employee: str):
    list_user_role = frappe.get_roles(frappe.session.user)
    list_transition, _, current_workflow_state = _get_list_transition(
        employee=employee)

    can_do_action = False

    for transition in list_transition:

        if ((transition.allowed in list_user_role) and (current_workflow_state == transition.state) and (transition.action == action)):
            can_do_action = True

        if can_do_action:
            break

    return can_do_action


@frappe.whitelist()
def cancel_probation_contract(contract_name):
    print('cancel_probation_contract')
    print(contract_name)
    if (not has_permission(core.CoreConstants.DoctypeName.Contract, ptype="write", raise_exception=False)):
        return

    if (not has_permission(core.CoreConstants.DoctypeName.Contract, ptype="read", raise_exception=False)):
        return

    contract_emp = frappe.db.get_value(
        core.CoreConstants.DoctypeName.Contract, contract_name, 'employee')

    if has_workflow_action_permission(action=EmployeeWorkflowAction.Absent.value, employee=contract_emp):
        # if has_workflow_action_permission(action=EmployeeWorkflowAction.Cancel.value, employee=contract_emp):

        frappe.db.set_value(core.CoreConstants.DoctypeName.Contract, contract_name, {
            'enable': 0,
            'status': ContractStatusMetadata.Cancelled.value,
            'is_primary': 0
        })
    return


@frappe.whitelist()
def generate_onboarding_agenda_template(employee):

    template = ''
    if has_workflow_action_permission(action=EmployeeWorkflowAction.SendAgenda.value, employee=employee):
        emp = frappe.get_doc(core.CoreConstants.DoctypeName.Employee, employee)

        template = frappe.db.get_value(
            core.CoreConstants.DoctypeName.EmailTemplate, EmailTemplate.OnboardingAgenda.value, 'response_html')

        context = {
            "candidate_name": emp.full_name,
            "event_hour": "09:00 AM",
            "event_date": "30/11/2022",
            "event_place": "Masteri Thao Dien",
            "conduct_date": "30/11/2022",
        }

        return get_jenv().from_string(template).render(context)

    return template


@frappe.whitelist()
def generate_offer_letter_template(employee: str, contract_id: str):

    def get_jwt_token(action: str):
        jwt_encode_data = {"email": frappe.session.user, "name": employee}
        if (action in [OfferLetterReplyActions.Accept.value, OfferLetterReplyActions.Negotiate.value, OfferLetterReplyActions.Deny.value]):
            jwt_encode_data['action'] = action

        token = jwt.encode(jwt_encode_data, JWT_SCRET_KEY,
                           algorithm=JWT_ALGORITHM)
        print('generate_offer_letter_template')
        print(token)
        return token

    # template = []
    template = ''
    if has_workflow_action_permission(action=EmployeeWorkflowAction.SendOffer.value, employee=employee) or has_workflow_action_permission(action=EmployeeWorkflowAction.ResendOffer.value, employee=employee):
        host_url = frappe.utils.get_url()
        emp = frappe.get_doc(core.CoreConstants.DoctypeName.Employee, employee)
        contract = frappe.get_doc(
            core.CoreConstants.DoctypeName.Contract, contract_id)

        template = frappe.db.get_value(
            core.CoreConstants.DoctypeName.EmailTemplate, EmailTemplate.OfferLetter.value, 'response_html')

        context = {
            "offer_letter_image_url": f"{host_url}/files/offer_letter_img.png",
            "candidate_name": emp.full_name,
            "position": contract.job_position,
            "salary": contract.wage,
            "working_hour": contract.working_schedule,
            "start_date": CoreUtils.format_date_dmy(contract.start_date),
            "supervisor_title": emp.get_report_line_title(langCode=LanguageCode.EN),
            "url_accept": f"{host_url}/offer_letter_reply?token={get_jwt_token(action=OfferLetterReplyActions.Accept.value)}",
            "url_negotiate": f"{host_url}/offer_letter_reply?token={get_jwt_token(action=OfferLetterReplyActions.Negotiate.value)}",
            "url_deny": f"{host_url}/offer_letter_reply?token={get_jwt_token(action=OfferLetterReplyActions.Deny.value)}",
        }

        return get_jenv().from_string(template).render(context)

    return template


def _send_email(email, message, subject):
    print(f"Send email to {email}, message = {message}")
    frappe.sendmail(
        recipients=email,
        subject=subject,
        message=message,
        now=True,
    )


@frappe.whitelist()
def send_email(
    recipients=None,
        subject="No Subject",
        message="No Message",
        attachments=None,
        cc=None,
        bcc=None,
):
    frappe.sendmail(
        recipients=recipients,
        subject=subject,
        message=message,
        now=True,
        cc=cc,
        bcc=bcc,
        attachments=attachments,
    )


@frappe.whitelist()
def get_list_enable_primary_contract(emp: str):
    list_contract = frappe.db.sql(
        """
            SELECT 
                name, employee, company, department, job_position, position_band, start_date, end_date, status, type, term, workflow_state, pdf_file, docx_file
            FROM `tabContract`
            WHERE 
                employee = '{emp}'
            AND status in ('{status_new}', '{status_running}')
            AND type = '{type_contract}'
            AND tabContract.is_primary_contract = '1'
            AND tabContract.enable = '1'
        """.format(emp=emp, status_new=ContractStatusMetadata.New.value, status_running=ContractStatusMetadata.Running.value, type_contract=ContractTypeMetadata.Contract.value),
        as_dict=True,
    )

    print('get_list_enable_primary_contract')
    print(list_contract)
    return list_contract


@frappe.whitelist()
def get_list_contract_attachments(emp: str) -> list:
    list_contract = frappe.db.sql(
        """
            select tF.file_name, tF.file_url, tF.is_private, tF.name
            from tabFile tF
            inner join tabContract tC
            on tC.name = tF.attached_to_name
            where tC.enable = '1' and tC.employee = %(emp)s
        """,
        values={
            'emp': emp
        },
        as_dict=True,
    )
    print('get_list_contract_attachments')
    print(list_contract)
    return list_contract


def _get_list_user_LDAP():
    url = "https://masterise-portal-api-ldap.masterisehomes.com:6789/ldapCRMListUser"
    params = {
        "USER": "admin",
        "PASSWORD": "cakEpoOlsTarhoMe",
        "IS_ACTIVE": "1",
        "IS_DISABLED": "0"
    }
    try:
        list_LDAP_user_response: Response = requests.post(
            url=url, json=params, verify=False, timeout=10)
        list_LDAP_user = json.loads(list_LDAP_user_response.text)
        return list_LDAP_user
    except BaseException as e:
        raise e


def _is_invalid_user_account(list_LDAP_user, user_account):
    if (list_LDAP_user):
        for user in list_LDAP_user["result"]:
            if user_account == user["sAMAccountName"]:
                return True
    return False


def _create_valid_user_account(first_name="", last_name="", middle_name=""):
    first_name = CoreUtils.convert_to_non_accent_vietnamese(first_name)
    last_name = CoreUtils.convert_to_non_accent_vietnamese(last_name)
    middle_name = CoreUtils.convert_to_non_accent_vietnamese(middle_name)

    user_account = f"{first_name}.{last_name}"
    list_LDAP_user = _get_list_user_LDAP()

    if _is_invalid_user_account(list_LDAP_user, user_account):
        user_account = f"{middle_name}{first_name}.{last_name}"
        if _is_invalid_user_account(list_LDAP_user, user_account):
            index = 1
            while True:
                user_account = f"{middle_name}{first_name}.{last_name}{index}"
                if _is_invalid_user_account(list_LDAP_user, user_account):
                    index += 1
                else:
                    break

    print(f"This User's Account is valid {user_account}")
    return user_account

###
### HANDLE WORKFLOW ACIONS ###
###

def _has_personal_id(employee_doc: Employee):
    personal_documents = employee_doc.personal_doc
    if (personal_documents):
        for i in range(len(personal_documents)):
            if (personal_documents[i].type in [PersonalDocumentTypeMetadata.CCCD.value, PersonalDocumentTypeMetadata.CMND.value, PersonalDocumentTypeMetadata.Passport.value]):
                if (personal_documents[i].number is not None and personal_documents[i].attached_file is not None):
                    return True
    return False


def _has_work_permit(employee_doc: Employee):
    personal_documents = employee_doc.personal_doc
    has_valid_visa = False
    has_valid_work_permit = False

    if (personal_documents):
        current_date = now()
        for i in range(len(personal_documents)):
            if (personal_documents[i].type == PersonalDocumentTypeMetadata.Visa):
                if (getdate(personal_documents[i].expiration_date) > current_date and personal_documents[i].attached_file is not None):
                    has_valid_visa = True

            elif (personal_documents[i].type == PersonalDocumentTypeMetadata.WorkPermit):
                if (getdate(personal_documents[i].expiration_date) > current_date and personal_documents[i].attached_file is not None):
                    has_valid_work_permit = True
    return has_valid_visa and has_valid_work_permit


def _can_create_job_offer(employee_doc: Employee):
    if ((employee_doc.nationality == "Vietnam" and _has_personal_id(employee_doc=employee_doc)) or _has_work_permit(employee_doc=employee_doc)):
        return True
    return False


def _get_list_transition_v2():
    employee_workflow = frappe.get_doc(
        core.CoreConstants.DoctypeName.Workflow, 'Employee Workflow')
    return employee_workflow.transitions, employee_workflow.states


def _update_employee_status(employee, current_state):
    employee_status = None

    emp = frappe.get_doc(core.CoreConstants.DoctypeName.Employee, employee)

    match (current_state):
        case EmployeeWorkflowState.InterviewCVAssessment.value:
            if (emp.last_name and emp.middle_name and emp.first_name and emp.private_email):
                employee_status = EmployeeStatusMetadata.Applicant.value

        case EmployeeWorkflowState.InterviewUnderInterview.value:
            employee_status = EmployeeStatusMetadata.Candidate.value

        case EmployeeWorkflowState.InterviewPassed.value:
            employee_status = EmployeeStatusMetadata.Preboarding.value

        case EmployeeWorkflowState.InterviewRejected.value:
            employee_status = EmployeeStatusMetadata.ApplicantJunk.value

        case EmployeeWorkflowState.OnboardOffering.value:
            employee_status = EmployeeStatusMetadata.Preboarding.value

        case EmployeeWorkflowState.OnboardOfferAccepting.value:
            employee_status = EmployeeStatusMetadata.Preboarding.value

        case EmployeeWorkflowState.OnboardOfferAccepted.value:
            employee_status = EmployeeStatusMetadata.Preboarding.value

        case EmployeeWorkflowState.OnboardOfferDenied.value:
            employee_status = EmployeeStatusMetadata.CandidateJunk.value

        case EmployeeWorkflowState.OnboardAbsence.value:
            employee_status = EmployeeStatusMetadata.Preboarding.value

        case EmployeeWorkflowState.OnboardShowingUp.value:
            employee_status = EmployeeStatusMetadata.Onboarding.value

        case EmployeeWorkflowState.OnboardInfoFilled.value:
            employee_status = EmployeeStatusMetadata.Onboarding.value

        case EmployeeWorkflowState.OnboardContractSigned.value:
            employee_status = EmployeeStatusMetadata.Onboarding.value

        case EmployeeWorkflowState.OnboardInProbation.value:
            employee_status = EmployeeStatusMetadata.InProbation.value

        case EmployeeWorkflowState.OnboardCancelled.value:
            employee_status = EmployeeStatusMetadata.CandidateJunk.value

    if (employee_status):
        frappe.db.set_value(core.CoreConstants.DoctypeName.Employee,
                            employee, "status", employee_status)


def _change_workflow_state(new_state: str, employee: str):
    frappe.db.set_value(core.CoreConstants.DoctypeName.Employee,
                        employee, "workflow_state", new_state)
    _update_employee_status(employee=employee, current_state=new_state)


def _cancel_all_contracts(employee: str):
    print('contract info')
    list_contract = frappe.db.get_list(
        core.CoreConstants.DoctypeName.Contract,
        filters={
            'employee': employee,
            'status': ['IN', [ContractStatusMetadata.New.value, ContractStatusMetadata.Running.value]],
        },
        fields=['name']
    )
    for i in range(len(list_contract)):
        frappe.db.set_value(core.CoreConstants.DoctypeName.Contract, list_contract[i]['name'], {
            'enable': 0,
            'status': ContractStatusMetadata.Cancelled.value,
            'is_primary': 0
        })


def _create_ldap_user_account(employee_doc):

    try:

        if (employee_doc.work_email):
            return True
        else:
            username = _create_valid_user_account(
                first_name=employee_doc.first_name, last_name=employee_doc.last_name, middle_name=employee_doc.middle_name)
            business_email = username + '@yopmail.com'

            user = frappe.new_doc("User")
            user.update(
                {
                    "email": business_email,
                    "enabled": 1,
                    "first_name": employee_doc.first_name,
                    "middle_name": employee_doc.middle_name,
                    "last_name": employee_doc.last_name,
                    "new_password": username
                }
            )
            user.insert()
            user.append('roles', {
                "doctype": core.CoreConstants.DoctypeName.HasRole,
                "role": "Candidate"
            })
            user.save(ignore_permissions=True)
            frappe.db.set_value(core.CoreConstants.DoctypeName.Employee,
                                employee_doc.name, "work_email", business_email)
            add_user_permission_for_employee(employee_doc.name)
            return True
    except:
        print('cannot create ldap user account')
        return False


def _get_enable_primary_probation_contract(employee):
    list_contract = frappe.db.get_list(
        core.CoreConstants.DoctypeName.Contract,
        fields=['name', 'company', 'department', 'job_position', 'position_band', 'start_date', 'end_date',
                'status', 'type', 'term', 'is_primary', 'enable', 'workflow_state', 'pdf_file'],
        filters={
            'employee': employee,
            'status': ContractStatusMetadata.Running,
            'type': ContractTypeMetadata.Contract,
            'term': ContractTermMetadata.Probation,
            'is_primary': 1,
            'enable': 1
        }
    )
    print('_get_enable_primary_probation_contract')
    print(list_contract)
    if (not list_contract):
        return None

    if (len(list_contract) != 1):
        return None

    return list_contract[0]


def _can_create_otp(contract_name: str):
    contract = frappe.get_doc(
        core.CoreConstants.DoctypeName.Contract, contract_name)

    if (contract and contract.otp_creation and contract.otp):
        # otp_life = (datetime.strptime(now(), '%Y-%m-%d %H:%M:%S.%f') - datetime.strptime(contract.otp_creation, '%Y-%m-%d %H:%M:%S.%f')).seconds
        otp_life = (datetime.now() - contract.otp_creation).seconds
        print(f"otp life = {otp_life}")
        if (otp_life < OTP_LIFE_DURATION_SECOND):
            # the current otp is still alive
            return False, contract.otp
        else:
            # the current otp is expired => clear it
            frappe.db.set_value(core.CoreConstants.DoctypeName.Contract, contract_name, {
                'otp': None,
                'otp_creation': None,
            }, update_modified=False)

            return True, None
    else:
        return True, None


def _sign_contract(employee_doc: Employee, submitted_otp: str):

    def _generate_signed_contract_pdf(full_file_name: str, content_url: str):
        if (content_url):
            response = requests.get(content_url)
            with open(full_file_name, 'wb') as f:
                f.write(response.content)

    def _get_new_signed_file_name(old_name: str):
        str_list = old_name.split(".")
        str_list[-2] += "_signed"
        return ".".join(str_list)

    def _remove_temp_public_contract(file_name: str):
        if (os.path.exists(file_name)):
            os.remove(file_name)

    def _extract_pdf_file_name(pdf_file: str):
        return pdf_file.split("/")[-1]

    def _get_company_name(company: str) -> str:
        return frappe.db.get_value(core.CoreConstants.DoctypeName.Company, company, 'full_name')

    def _attach_employee_signature(contract: Contract, otp: str, employee_email: str) -> str:
        url = f"{core.CoreConstants.APIDomain.DigiSign}{core.CoreConstants.APIPath.CustomerSign}"
        params = {
            "USER": "user",
            "PASSWORD": "hhiuwYhsAYqhqwSJjsHUSUHQauhUQH6623545123884776235",
            "OTP": otp,
            "Customer_Name": contract.employee_name,
            "Customer_ID": contract.employee,
            "Customer_DocID": f"HCMS-{contract.name}",
            "Customer_Company": _get_company_name(company=contract.company),
            "Customer_Email": employee_email,
            "URL_Source": f"{frappe.utils.get_url()}/files/temp/{contract.employee}/{_extract_pdf_file_name(pdf_file=contract.pdf_file)}",
            "Pattern": "@TEST1@"
        }
        try:
            response: Response = requests.post(
                url=url, json=params, verify=False, timeout=10)
            print('_attach_employee_signature')
            print(response.text)
            # list_LDAP_user = json.loads(list_LDAP_user_response.text)
            # return list_LDAP_user
            return ''
        except BaseException as e:
            raise e

    def _attach_company_signature(company: str, otp: str, url_source: str) -> str:
        # "https://digisign-crm-new.masterisehomes.com:10001/freeDigiSign_CRM_New_Company_Req"
        url = f"{core.CoreConstants.APIDomain.DigiSign}{core.CoreConstants.APIPath.CopmpanySign}"
        params = {
            "USER": "user",
            "PASSWORD": "hhiuwYhsAYqhqwSJjsHUSUHQauhUQH6623545123884776235",
            "OTP": otp,
            "Company": _get_company_name(company=company),
            "URL_Source": url_source,
            "Pattern": "@TEST2@"
        }
        try:
            response: Response = requests.post(
                url=url, json=params, verify=False, timeout=10)
            print('_attach_company_signature')
            print(response.text)
            return ''
        except BaseException as e:
            raise e

    # TODO: check response of these 2 APIs

    def _attach_signatures(otp: str, contract_id: str, employee_email: str):
        contract = frappe.get_doc(
            core.CoreConstants.DoctypeName.Contract, contract_id)
        employee_signed_pdf_url = _attach_employee_signature(
            contract=contract, otp=otp, employee_email=employee_email)
        company_signed_pdf_url = _attach_company_signature(
            company=contract.company, otp=otp, url_source=employee_signed_pdf_url)
        return company_signed_pdf_url

    def _generate_temp_public_contract(contract_id: str) -> str:
        contract = frappe.get_doc(
            core.CoreConstants.DoctypeName.Contract, contract_id)
        temp_pdf_path = contract.generate_temp_public_contract()
        return temp_pdf_path

    def _attach_digital_signature(probation_contract_id: str, employee_email: str, otp: str):
        contract = frappe.get_doc(
            core.CoreConstants.DoctypeName.Contract, probation_contract_id)
        temp_file_path = _generate_temp_public_contract(
            contract_id=probation_contract_id)
        signed_file_response = _attach_signatures(
            otp=otp, contract_id=probation_contract_id, employee_email=employee_email)

        contract_pdf_file_name = _extract_pdf_file_name(
            pdf_file=contract.pdf_file)
        _remove_temp_public_contract(
            file_name=f"{temp_file_path}/{contract_pdf_file_name}")
        signed_file_name = _get_new_signed_file_name(
            old_name=contract_pdf_file_name)
        _generate_signed_contract_pdf(
            full_file_name=f"{temp_file_path}/{signed_file_name}", content_url=signed_file_response)

    probation_contract = _get_enable_primary_probation_contract(
        employee=employee_doc.name)

    if (not probation_contract):
        return False, Messages.NoPrimaryContractFound.value

    should_create_otp, current_otp = _can_create_otp(
        contract_name=probation_contract.name)
    print(
        f'should_create_otp = {should_create_otp}, current_otp = {current_otp}')
    if (not should_create_otp):
        if (current_otp == submitted_otp):
            _attach_digital_signature(probation_contract_id=probation_contract.name,
                                      employee_email=employee_doc.work_email, otp=current_otp)
            return True, None
        return False, Messages.IncorrectOTP.value
    else:
        _send_otp(employee_doc=employee_doc,
                  contract_id=probation_contract.name, send_via_sms=True)
        return False, Messages.CheckEmailOTP.value


def _is_valid_action_workflow(employee_doc: Employee, action: str):

    list_user_role = frappe.get_roles(frappe.session.user)
    list_transition, _ = _get_list_transition_v2()
    is_action_valid = False
    valid_transition = None

    for transition in list_transition:
        if ((transition.allowed in list_user_role) and (employee_doc.workflow_state == transition.state) and (action == transition.action)):
            is_action_valid = True
            valid_transition = transition
            break

    return is_action_valid, valid_transition


def _send_otp(employee_doc: Employee, contract_id: str, send_via_sms: bool = False, send_via_email: bool = True):
    def _gen_otp() -> str:
        string = '0123456789'
        otp = ""
        length = len(string)
        for _ in range(6):
            otp += string[int(math.floor(random.random() * length))]
        return otp

    def _send_sms(otp: str, phones: list[str] = []) -> None:
        url = f"{core.CoreConstants.APIDomain.SMS}{core.CoreConstants.APIPath.SMS}"
        print(url)
        formatted_phones = []
        for i in range(len(phones)):
            formatted_phones.append(CoreUtils.reformat_phone_number(phones[i]))
        params = [{
            "phones": formatted_phones,
            "message": f"{Messages.MessageOTP.value} {otp}",
            "service": OTPSMSServiceEnv.Dev.value
        }]
        try:
            sign_otp_response: Response = requests.post(
                url=url, json=params, verify=False, timeout=10, auth=("admin", "cakebig2021"))
            print(sign_otp_response.text)
            if (sign_otp_response.status_code != core.CoreConstants.APIStatusCode.Success):
                raise core_exception.OTPMessageException(
                    message=sign_otp_response.text)
        except BaseException as e:
            print(e)
            raise e

    otp = _gen_otp()

    frappe.db.set_value(
        core.CoreConstants.DoctypeName.Contract,
        contract_id,
        {
            'otp': otp,
            'otp_creation': datetime.now(),
        }
    )

    if (send_via_email):
        _send_email(email=employee_doc.private_email,
                    message=f"{Messages.MessageOTP} {otp}", subject="Contract Digital Signature")

    if (send_via_sms):
        _send_sms(otp=otp, phones=[employee_doc.private_phone])


def __is_in_onboard_event(employee_doc : Employee) -> bool:
    list_event_onboard = employee_doc.get_list_events_with_type('Onboard')
    if not (list_event_onboard):
        return False

    if (len(list_event_onboard) != 1):
        return False

    return True
    



@frappe.whitelist()
def send_otp_to_candidate(employee: str, action: str, document_id: str):

    try:
        employee_doc = frappe.get_doc(
            core.CoreConstants.DoctypeName.Employee, employee)
        is_valid_action, _ = _is_valid_action_workflow(
            employee_doc=employee_doc, action=action)
        print(f'action = {action}, is valid = {is_valid_action}')

        if (is_valid_action):
            if (action == EmployeeWorkflowAction.SignContract.value):
                should_create_otp, _ = _can_create_otp(
                    contract_name=document_id)
                print(f'should_create_otp = {should_create_otp}')
                if (should_create_otp):
                    _send_otp(employee_doc=employee_doc,
                              contract_id=document_id, send_via_sms=True)
                    return True
                return False
            return False
    except BaseException as e:
        print(e)
        return False


@frappe.whitelist()
def handle_workflow_action(action: str, employee: str, submitted_otp: str = ''):
    callback_msg = None
    try:
        employee_doc = frappe.get_doc(
            core.CoreConstants.DoctypeName.Employee, employee)
        is_valid_action, transition = _is_valid_action_workflow(
            employee_doc=employee_doc, action=action)
        print(
            f'handle_workflow_action action = {action}, is valid = {is_valid_action}')

        if (is_valid_action):
            match (action):
                case EmployeeWorkflowAction.Shortlist.value:
                    _change_workflow_state(
                        new_state=transition.next_state, employee=employee)

                case EmployeeWorkflowAction.Pass.value:
                    _change_workflow_state(
                        new_state=transition.next_state, employee=employee)

                case EmployeeWorkflowAction.Reject.value:
                    _change_workflow_state(
                        new_state=transition.next_state, employee=employee)
                case EmployeeWorkflowAction.SendOffer.value:
                    _change_workflow_state(
                        new_state=transition.next_state, employee=employee)

                case EmployeeWorkflowAction.ResendOffer.value:
                    _change_workflow_state(
                        new_state=transition.next_state, employee=employee)

                case EmployeeWorkflowAction.Absent.value:
                    _cancel_all_contracts(employee=employee)
                    _change_workflow_state(
                        new_state=transition.next_state, employee=employee)

                case EmployeeWorkflowAction.UpdateOfferLetter.value:
                    notify_message = f'The offer letter of candidate {employee_doc.name} is required to be updated.'
                    print('handle update offer letter')
                    _notify_hiring_manager(message=notify_message)
                    _change_workflow_state(
                        new_state=transition.next_state, employee=employee)

                case EmployeeWorkflowAction.ConfirmShowingUp.value:
                    notify_message = f'Candidate {employee_doc.name} is attending the Onboarding date.'
                    _create_ldap_user_account(employee_doc=employee_doc)
                    _generate_employee_code(employee=employee)
                    _change_workflow_state(
                        new_state=transition.next_state, employee=employee)
                    _notify_hiring_manager(message=notify_message)

                case EmployeeWorkflowAction.SendAgenda.value:
                    if (__is_in_onboard_event(employee_doc=employee_doc)):
                        _change_workflow_state(
                            new_state=transition.next_state, employee=employee)

                case EmployeeWorkflowAction.Submit.value:
                    _change_workflow_state(
                        new_state=transition.next_state, employee=employee)

                case EmployeeWorkflowAction.SignContract.value:
                    print(f"submitted otp = {submitted_otp}")
                    result, message_response = _sign_contract(
                        employee_doc=employee_doc, submitted_otp=submitted_otp)
                    print(
                        f"EmployeeWorkflowAction.SignContract result = {result}")
                    if (result):
                        _change_workflow_state(
                            new_state=transition.next_state, employee=employee)
                        return True, None
                    else:
                        callback_msg = message_response
                        return False, callback_msg
                case EmployeeWorkflowAction.ConfirmInProbation.value:
                    _change_workflow_state(
                        new_state=transition.next_state, employee=employee)

                case EmployeeWorkflowAction.Cancel.value:
                    _cancel_all_contracts(employee=employee)
                    _change_workflow_state(
                        new_state=transition.next_state, employee=employee)
            return True, None
        else:
            return False, callback_msg
    except BaseException as e:
        print(e)
        callback_msg = str(e)
        return False, callback_msg


@frappe.whitelist()
def search_hr_employee(doctype, txt, searchfield, start, page_len, filters):
    print(txt)
    print(start)
    return frappe.get_list(doctype=doctype, fields=['name', 'full_name'], filters={
        'status': ['IN', [EmployeeStatusMetadata.Active.value, EmployeeStatusMetadata.OnLeaves.value]],
        searchfield: ['LIKE', f"%{txt}%"],
        'department': 'HRAD'
    }, limit_start=start,
        limit_page_length=page_len, as_list=True)
