function buildTabComponent(wrapper, listItem) {
    wrapper.append(frappe.render_template('page_tab_view', {
        tabs: listItem
    }))
    $(document).ready(() => {
        $('.tab').click((e) => {
            $('.tab').removeClass("active")
            $(e.target).addClass("active")
            let indexActive = $(e.target).attr("data-rel") - 1
            listItem[indexActive].handle()
            let slidenum = 120 * indexActive
            $(".tab-indicator").css("left", slidenum)
        })
        $('#tab-0').trigger('click')
    })
}

function replaceTabContent(buildContent) {
    let wrapper = $('#tab-content').empty()
    wrapper.append(buildContent(wrapper))
}
