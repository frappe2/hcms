async function buildSideNavBar(wrapper, currentWorkspace) {
    const { pages } = await frappe.xcall(
        "frappe.desk.desktop.get_workspace_sidebar_items"
    )
    wrapper.append(frappe.render_template('side_nav_bar', {
        currentWorkspace: currentWorkspace, pages: pages
    }))
}