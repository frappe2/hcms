import frappe
from frappe.utils import getdate

def update_business_representative_status(today_date):
    list_business_representative = frappe.db.get_list(
        'Business Representative',
        filters={
            'status': 'Active',
        },
        fields = ['valid_from', 'valid_to', 'br_name_comp_role'],
    )
    if (len(list_business_representative) > 0):
        for business_representative in list_business_representative:
            if not ((business_representative['valid_from'] <= today_date and business_representative['valid_to'] >= today_date)
            or (business_representative['valid_from'] <= today_date and business_representative['valid_to'] is None)):
                frappe.db.set_value('Business Representative', business_representative['br_name_comp_role'], 'status', 'Inactive')


def terminate_employee_status(today_date):
    list_employee = frappe.db.get_list(
        'Employee',
        filters={
            'status': ['IN', ['Active', 'In Probation']],
        },
        fields = ['terminate_last_working_date', 'name'],
    )
    for emp in list_employee:
        if (emp['terminate_last_working_date'] is not None):
            if (today_date > emp['terminate_last_working_date']):
                frappe.db.set_value('Employee', emp['name'], 'status', 'Terminated')


def daily():
    today_date = getdate()
    update_business_representative_status(today_date)

    
				
