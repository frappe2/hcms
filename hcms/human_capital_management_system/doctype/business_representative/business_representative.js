// Copyright (c) 2022, masterisehomes and contributors
// For license information, please see license.txt


var role_metadata = {
    BR: 'Business Representative',
    AP: 'Authorizing Person'
}

function get_first_words(text){
    let res = ''
    console.log(text.split(" "))
    text.split(" ").forEach(element => {
        res = res + element.charAt(0)
    });
    return res
}


function update_br_name_company(frm){
    let name = frm.doc.full_name_display ? frm.doc.full_name_display : ''
    let comp = frm.doc.company ? get_first_words(frm.doc.company) : ''  
    let role = frm.doc.role ? get_first_words(frm.doc.role) : ''  
    frm.set_value('br_name_comp_role', name.concat('-', comp, '-', role))  

    // frm.set_value('br_name_comp_role', name.concat(' - ', comp)) 
}

frappe.ui.form.on('Business Representative', {

    onload: function(frm){
        frm.toggle_display(['authorized_by'], frm.doc.role == role_metadata.AP)
        frm.toggle_reqd(['authorized_by'], frm.doc.role == role_metadata.AP)
        update_br_name_company(frm)
    },

    br_name: function(frm){
        update_br_name_company(frm)
    },

    company: function(frm){
        update_br_name_company(frm)
    },

    role: function(frm){
        frm.toggle_display(['authorized_by'], frm.doc.role == role_metadata.AP)
        frm.toggle_reqd(['authorized_by'], frm.doc.role == role_metadata.AP)

        update_br_name_company(frm)
    },


	valid_from: function(frm){
        let date_now = frappe.datetime.get_today()

        /// Valid From must has value
        if (frm.doc.valid_from){
            if (frm.doc.valid_to){
                // Throw exception if valid_from > valid_to
                if (frm.doc.valid_from > frm.doc.valid_to){
					frm.set_value('status', '')
					frm.set_value('valid_from', '')
                    frappe.throw('Valid From must be less than or equal to Valid To')
                }
                else{
                    if ((frm.doc.valid_from <= date_now) && (date_now <= frm.doc.valid_to)){
						frm.set_value('status', 'Active')
                    }else{
						frm.set_value('status', 'Inactive')
                    }
                }
            }else{
                if (frm.doc.valid_from <= date_now){
                    frm.set_value('status', 'Active')
                }else{
                    frm.set_value('status', 'Inactive')
                }
            }
        }
        /// Valid From has no data => reset status to be empty
        else{
            frm.set_value('status', '')
        }      
    },
    
    valid_to: function(frm){
        let date_now = frappe.datetime.get_today()
        if (frm.doc.valid_from){
            // throw exception if valid_from > valid_to
            if (frm.doc.valid_from > frm.doc.valid_to){
				frm.set_value('valid_to', '')
                frappe.throw('Valid To must be the date after Valid From')
            }
            else{
                if (
                    ((frm.doc.valid_from <= date_now) && (date_now <= frm.doc.valid_to))
                    || ((frm.doc.valid_from <= date_now) && (!frm.doc.valid_to))){
					frm.set_value('status', 'Active')
                }else{
					frm.set_value('status', 'Inactive')
                }
            }
        }
        
    }
});
