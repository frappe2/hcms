frappe.provide("frappe.node_modules.frappe-datatable.dist.frappe-datatable.js")

frappe.pages['test-hr-page'].on_page_load = function(wrapper) {
	// new Page(wrapper);
	const test_hr_page = new TestHRPage(wrapper);
}

class TestHRPage{
	constructor(wrapper){
		this.page = frappe.ui.make_app_page({
			parent: wrapper,
			title: 'Page 1',
			single_column: true
		});

		this.wrapper = wrapper;

		console.log('wrapper')
		console.log(this.wrapper)
 
		this.workspace = "HCMS";

		this.make();
	}

	async make(){

		this.page.add_inner_button(
			'Tab 1', () => this.show_tab_1()
		)

		this.page.add_inner_button(
			'Tab 2', () => this.show_tab_2()
		)

		const base_url = window.location.protocol.concat('//', window.location.host)
		const tab_1_url = base_url.concat("/app/query-report/test-report-hr") 
		const tab_2_url = base_url.concat("/app/test-tab-2-hr-page")
	

		const iframeHTML_tab_1 = `<iframe style="width: 100%; height: calc(100vh - 150px); border: none; border-radius: 10px" referrerpolicy="unsafe-url" frameBorder="0" src="${tab_1_url}"></iframe>`;
		const iframeHTML_tab_2 = `<iframe style="width: 100%; height: calc(100vh - 150px); border: none; border-radius: 10px" referrerpolicy="unsafe-url" frameBorder="0" src="${tab_2_url}"></iframe>`;

		let tab_1_html = $(frappe.render_template(iframeHTML_tab_1, this))
	
	}

	

	show_tab_1(){
		$(frappe.render_template(this.iframeHTML_tab_1, this)).appendTo(this.page.main);
	}

	show_tab_2(){
		$(frappe.render_template(this.iframeHTML_tab_2, this)).appendTo(this.page.main);
	}

}