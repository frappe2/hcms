# Copyright (c) 2022, masterisehomes and contributors
# For license information, please see license.txt

import frappe

from frappe.model.document import Document

class Department(Document):
	
	def onload(self):
		list_employee = frappe.db.get_list('Employee',
			filters={
				'department': self.name
			},
			fields=['full_name'],
		)

		self.no_employee = len(list_employee)


