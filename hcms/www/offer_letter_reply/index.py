import frappe
import jwt
from hcms.config.constant import *
from hcms.human_capital_management_system.doctype.employee.employee import Employee

def get_context(context):

    def handle_reply_action(action : str, emp : Employee, sender : str):
        reply_email_message = ''
        host_url = frappe.utils.get_url()
        match action:
            case OfferLetterReplyActions.Accept.value:
                frappe.db.set_value('Employee', emp.name, "workflow_state", EmployeeWorkflowState.OnboardOfferAccepted.value)
                frappe.db.commit()
                context.image_url = f"{host_url}/files/offer_letter_reply_accept.png"
                context.offer_status = "Accepted"
                context.message = "You have accepted Offer Letter successfully. Masterise Homes will send you onboarding agenda shortly. Please check your email regularly for our latest information."
                
                reply_email_message = f"Candidate {emp.full_name} accepted the job offer."
            case OfferLetterReplyActions.Deny.value:
                frappe.db.set_value('Employee', emp.name, "workflow_state", EmployeeWorkflowState.OnboardOfferDenied.value)
                frappe.db.commit()
                context.image_url = f"{host_url}/files/offer_letter_reply_reject.png"
                context.offer_status = "Rejected"
                context.message = "You have rejected Offer Letter. In case of mistake, please contact Masterise Homes via Hotline ... or email ... for timely support."

                reply_email_message = f"Candidate {emp.full_name} denied the job offer."
            case OfferLetterReplyActions.Negotiate.value:
                frappe.db.set_value('Employee', emp.name, "workflow_state", EmployeeWorkflowState.OnboardOffering.value)
                frappe.db.commit()
                context.image_url = f"{host_url}/files/offer_letter_reply_negotiate.png"
                context.offer_status = "To Be Discussed"
                context.message = "For further discussion on Offer Letter, Masterise Homes will contact you shortly or you can reach out to us via Hotline .... or Email...."

                reply_email_message = f"Candidate {emp.full_name} would like to negotiate the job offer."

        # send reponse email back to the sender
        frappe.sendmail(
            recipients='phamhbk97@gmail.com',#sender,
            sender=emp.private_email,
            subject="Reply of the offer letter",
            message=reply_email_message,
            now=True
        )
    
    token = frappe.request.args["token"]
    payload = jwt.decode(token, JWT_SCRET_KEY, options={"verify_signature": False}, algorithms=JWT_ALGORITHM) or {}

    max_jwt_keys_number = 3
    list_payload_keys = list(payload.keys())

    if (len(list_payload_keys) != max_jwt_keys_number):
        context.result = 0
        return context

    if ((OfferLetterJWTKeys.Email.value not in list_payload_keys)
        or (OfferLetterJWTKeys.Name.value not in list_payload_keys)
        or (OfferLetterJWTKeys.Action.value not in list_payload_keys)):
        context.result = 0
        return context

    user = frappe.get_doc('User', payload[OfferLetterJWTKeys.Email.value])
    candidate = frappe.get_doc('Employee', payload[OfferLetterJWTKeys.Name.value])

    if (user and candidate):
        list_role = frappe.get_roles(user.email)
        if ((ROLE_CREATE_OFFER_LETTER in list_role) and (candidate.workflow_state == EmployeeWorkflowState.OnboardOfferAccepting.value)):
            action = payload[OfferLetterJWTKeys.Action.value]
            context.action = action
            handle_reply_action(action=action, emp=candidate, sender=payload[OfferLetterJWTKeys.Email.value])
                
    return context
            

    

