frappe.pages['menu-employee'].on_page_load = function (wrapper) {
	new MenuEmployeePage(wrapper, 'Employees')
}

class MenuEmployeePage {
	constructor(wrapper, workspace) {
		this.page = frappe.ui.make_app_page({
			parent: wrapper,
			title: 'Employee',
			single_column: false
		});
		this.workspace = workspace
		this.make()
		frappe.provide('appcore.util')
		appcore.util.newDocumentDoctypeButton = this.newDocumentDoctypeButton
		appcore.util.refreshButton = this.refreshButton
	}

	async make() {

		buildSideNavBar(this.page.sidebar.empty(), this.workspace)

		buildTabComponent(this.page.main, [
			{
				name: 'Employee',
				handle: () => replaceTabContent(this.buildEmployeeComponent)
			},
			{
				name: 'Leave',
				handle: () => replaceTabContent(this.buildLeaveComponent)
			}
		])
	}

	newDocumentDoctypeButton(doctype, label) {
		let button = document.createElement("button")
		button.className = "btn btn-primary btn-sm primary-action"
		button.innerHTML = `<svg class="icon icon-xs" style="">
					<use class="" href="#icon-add"></use>
				</svg> <span class="hidden-xs" data-label="${label}"> <span class="alt-underline">${label}</span>`
		button.onclick = function () {
			frappe.new_doc(doctype)
		}
		return button
	}

	refreshButton(callback) {
		let button = document.createElement("button")
		button.className = "text-muted btn btn-default icon-btn"
		button.innerHTML = `<svg class="icon icon-sm" style="">
			<use class="" href="#icon-refresh"></use></svg>`
		button.onclick = function () { callback() }
		return button
	}

	buildLeaveComponent(wrapper) {
		const url = `https://dev-congthongtin.masterisehomes.com/admin/authentication/loginApi?token=${window
			.localStorage
			.getItem("token_portal")
			}&url=/admin/hrtakeleave/index?source=hcms&langBitrix=en`
		const iframeHTML = `<iframe style = "width: 100%; height: calc(100vh - 150px); border: none; border-radius: 10px" referrerpolicy = "unsafe-url" frameBorder = "0" src = "${url}" ></iframe>`
		wrapper
			.append(iframeHTML)
			.show()
	}

	buildEmployeeComponent(wrapper) {

		let standardActions = document.createElement("div")
		standardActions.className = "page-actions flex col justify-content-end"
		standardActions.append(appcore.util.refreshButton(() => { fetchData() }), appcore.util.newDocumentDoctypeButton('Employee', 'Add Employee'))

		let searchAction = document.createElement("div")

		let datatableActions = document.createElement("div")
		datatableActions.className = "row flex justify-between"
		datatableActions.append(searchAction, standardActions)

		wrapper.append(datatableActions)

		wrapper.append(`
		<div id="data-table"></div>	
		`)

		const columns = [
			{
				name: 'Name', focusable: false, editable: false, id: 'name', width: 1,
				format: (value) => {
					let arr = value.split('::')
					let linkEmployee = Object.assign(document.createElement("a"), {
						href: `/app/${frappe.router.slug('Employee')}/${encodeURIComponent(cstr(arr[1]))}`,
						innerText: arr[0],
						className: "bold"
					})
					return linkEmployee.outerHTML
				}
			}, { name: 'Work phone', id: 'work_phone', editable: false, focusable: false, width: 1 }, {
				name: 'Work email', id: 'work_email', editable: false, focusable: false, width: 1
			}, { name: 'Company', id: 'company', editable: false, focusable: false, width: 1 },
			{ name: 'Address', id: 'address', editable: false, width: 2 },
			{
				name: 'Status', id: 'status', editable: false, focusable: false, width: 1, format: (value) => {
					let result = document.createElement("div")
					result.innerText = value?.toUpperCase()
					result.className = "badge badge-pill"
					switch (value) {
						case EmployeeStatusMetadata.Applicant:
							result.style.color = "#B57808"
							break
						case EmployeeStatusMetadata.Active:
							result.style.color = "#638719"
							break
						case EmployeeStatusMetadata.CandidateJunk:
							result.style.color = "#FA3535"
							break
					}
					return result.outerHTML
				}
			},
			{
				name: 'Date created', id: 'date_created', editable: false, focusable: false, width: 1
			},
			// {
			// 	name: 'Action', editable: false, focusable: false, format: (value) => {
			// 		let listButton = []
			// 		listButton.push(Object.assign(document.createElement("button"), {
			// 			innerText: 'Edit',
			// 			className: 'btn btn-primary'
			// 		}).outerHTML)
			// 		listButton.push(Object.assign(document.createElement("button"), {
			// 			innerText: 'Delete',
			// 			className: 'btn btn-danger'
			// 		}).outerHTML)
			// 		listButton.push(Object.assign(document.createElement("button"), {
			// 			innerText: 'Duplicate',
			// 			className: 'btn btn-warning'
			// 		}).outerHTML)
			// 		return `<div>${listButton.join('')}</div>`
			// 	}
			// }
		]

		const datatable = new DataTable(`#${wrapper.get(0).id} #data-table`, {
			columns: columns,
			data: [],
			layout: 'ratio',
			disableReorderColumn: true,
			serialNoColumn: false,
			inlineFilters: true,
			checkboxColumn: true
		})

		function fetchData(filters) {
			frappe.call({
				method: "hcms.api.v1.employees.get_all",
				type: "GET",
				callback: (r) => {
					datatable.refresh(r.message)
				},
				error: (e) => {
					console.log(e)
				}
			})
		}

		fetchData()
	}
}

