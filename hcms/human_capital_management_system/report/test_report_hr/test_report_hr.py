# Copyright (c) 2022, masterisehomes and contributors
# For license information, please see license.txt

import frappe


def execute(filters=None):
	print('filters report')
	print(filters)

	columns = [
       {
			'fieldname': "company",
			'fieldtype': "Link",
			'label': "Company",
			'options': "Company"
		},
		{
			'fieldname': "position_band",
			'fieldtype': "Link",
			'label': "Position Band",
			'options': "Position Band"
		}
    ]
	data = frappe.db.get_all('Employee', ['company','position_band'])
	
	return columns, data
