import frappe
from frappe import _
from hcms.config.constant import EmployeeStatusMetadata


@frappe.whitelist()
def get_list_employee_active():
    frappe.get_list("Employee", filter={
        "status": EmployeeStatusMetadata.Active
    }, fields=["name", "full_name", "code", "email"])


@frappe.whitelist(methods="GET")
def get_all(filters=None):
    try:
        if frappe.has_permission(doctype="Page", doc="menu-employee"):
            data = frappe.db.sql(query="""
			SELECT CONCAT_WS('::', full_name, tabEmployee.name), work_phone, work_email, tabCompany.short_name, address, status, DATE_FORMAT(tabEmployee.creation, '%d-%m-%Y %H:%i') FROM tabEmployee LEFT JOIN tabCompany ON company = tabCompany.code;
		    """
                                 )
            return data
        else:
            frappe.throw(_("Not permitted"), frappe.PermissionError)
    except Exception as e:
        return e
