frappe.pages['attendant-management'].on_page_load = function(wrapper) {
	new Page(wrapper);
}

Page = Class.extend({
	init: function (wrapper) {
		this.page = frappe.ui.make_app_page({
			parent: wrapper,
			title: "Attendant Management",
			single_column: false,
		});

		this.workspace = "HCMS";

		this.make();
	},

	destroy: function (wrapper) {
		console.log("destroyed");
	},

	make: async function () {
		const url = `https://dev-congthongtin.masterisehomes.com/admin/authentication/loginApi?token=${window.localStorage.getItem("token_portal")}&url=/admin/hrreportfingerprint/index?source=hcms&langBitrix=en`;
		const iframeHTML = `<iframe style="width: 100%; height: calc(100vh - 150px); border: none; border-radius: 10px" referrerpolicy="unsafe-url" frameBorder="0" src="${url}"></iframe>`;
		const { pages } = await this.get_pages();
		let sidebarHTML = ""
		for (const item of pages) {
			sidebarHTML += `<div class="sidebar-item-container is-draggable" item-parent="" item-name="HCMS" item-public="${item.public}">
								<div class="desk-sidebar-item standard-sidebar-item ${item.label == this.workspace ? "selected" : ""}">
									<a href="/app/${
										item.public
											? frappe.router.slug(item.title)
											: "private/" + frappe.router.slug(item.title)
									}" class="item-anchor" title="HCMS">
										<span class="sidebar-item-icon" item-icon="share"><svg class="icon icon-md" style="">
												<use class="" href="#icon-${item.icon}"></use>
											</svg></span>
										<span class="sidebar-item-label">${item.label}<span>
											</span></span></a>
								</div>
								
								<div class="sidebar-child-item nested-container"></div>
							</div>`
		}
		$(frappe.render_template(iframeHTML, this)).appendTo(this.page.main);
		this.page.sidebar.empty().append(`
		<div class="list-sidebar overlay-sidebar hidden-xs hidden-sm">
			<div class="desk-sidebar list-unstyled sidebar-menu">
				<div class="standard-sidebar-section nested-container hidden" data-title="My Workspaces">
					<div class="standard-sidebar-label">
						<span><svg class="icon icon-xs" style="">
								<use class="" href="#icon-small-down"></use>
							</svg></span>
						</span></span>
					</div>
				</div>
				<div class="standard-sidebar-section nested-container" data-title="Public">
					${sidebarHTML}
				</div>
			</div>
		</div>
		`)
	},

	get_pages: async function() {
		return frappe.xcall("frappe.desk.desktop.get_workspace_sidebar_items");
	}



});

var sidebarHTML = `
<div class="list-sidebar overlay-sidebar hidden-xs hidden-sm">
	<div class="desk-sidebar list-unstyled sidebar-menu">
		<div class="standard-sidebar-section nested-container hidden" data-title="My Workspaces">
			<div class="standard-sidebar-label">
				<span><svg class="icon  icon-xs" style="">
						<use class="" href="#icon-small-down"></use>
					</svg></span>
				</span></span>
			</div>
		</div>
		<div class="standard-sidebar-section nested-container" data-title="Public">
			<div class="standard-sidebar-label">
				<span><svg class="icon  icon-xs" style="">
						<use class="" href="#icon-small-down"></use>
					</svg></span>
				<span class="section-title">Public<span>
					</span></span>
			</div>
		</div>
	</div>
</div>
`