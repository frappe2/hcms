# Copyright (c) 2022, masterisehomes and contributors
# For license information, please see license.txt

import requests
from requests import Response
import json
from dataclasses import fields
from email import message
from urllib import request
from venv import create
import frappe
from frappe.model.document import Document
import string
import random
import jinja2
from hcms_core.utils.utils import CoreUtils
import codecs
import os
import requests
import math
from frappe.utils import now, getdate, get_site_name
import re
from frappe.permissions import (
    add_user_permission,
    has_permission,
)
from hcms.config.constant import *
from hcms_contract.config.constant import *
import jwt
from datetime import datetime
import hcms_core.utils.constants as core


class Employee(Document):

    back_from_contract = True

    list_active_contract = []
    list_contract = []

    def _get_unit_date_with_plural(self, unit: str, quantity: int):

        if (quantity == 1):
            return unit

        return unit + 's'

    def update_company_address(self, company):
        if (company):
            company_address = frappe.db.get_value(
                core.CoreConstants.DoctypeName.Company, company, 'address')
            print(company_address)
            self.company_address = company_address
        else:
            self.company_address = None

    def update_head_of_department(self, department: str):
        if (department):
            list_hod = frappe.db.get_list(
                core.CoreConstants.DoctypeName.Department,
                filters={
                    'name': department,
                },
                fields=['head_of_department'],
            )
            if (len(list_hod) == 1):
                hod_full_name = frappe.db.get_value(
                    core.CoreConstants.DoctypeName.Employee, list_hod[0]['head_of_department'], 'full_name')
                print(hod_full_name)
                self.head_of_department = hod_full_name
        else:
            self.head_of_department = None

    def get_seniority(self, hired_date, end_date):
        days_per_month: int = 30

        seniority = end_date - hired_date
        # seniority_in_sec = seniority / 1000
        seniority_in_sec = seniority.total_seconds()

        if (seniority_in_sec == 0):
            return '< 1 day'

        days = seniority_in_sec / (24 * 3600)

        if (days < days_per_month):
            return str(days) + ' ' + self._get_unit_date_with_plural('day', days)

        months = days / days_per_month
        months_floor = math.floor(months)
        remain_days = math.floor((months - months_floor) * days_per_month)

        return str(months_floor) + ' ' + self._get_unit_date_with_plural('month', months_floor) + ' ' + str(remain_days) + ' ' + self._get_unit_date_with_plural('day', remain_days)

    def update_seniority(self, contract_hired_date, contract_end_date, termination_date):
        if (contract_hired_date is not None and contract_end_date is not None):
            end_date = getdate()

            for date in [contract_end_date, termination_date]:
                if (date is not None and end_date >= getdate(date)):
                    end_date = date

            self.seniority = self.get_seniority(
                getdate(contract_hired_date), end_date)
        else:
            self.seniority = None

    def get_list_contract(self):
        print('get_list_contract')
        print(self.name)
        list_contract = frappe.db.get_list(
            core.CoreConstants.DoctypeName.Contract,
            filters={
                'employee': self.name,
            },
            fields=['name', 'company', 'department', 'job_position', 'position_band', 'start_date', 'end_date',
                    'status', 'type', 'term', 'is_primary', 'enable', 'workflow_state'],
        )
        return list_contract

    def has_valid_probation_contract(self, list_contract):
        for contract in list_contract:
            if (contract['enable']
                and contract['is_primary']
                and contract['term'] == ContractTermMetadata.Probation.value
                and (contract['status'] in [ContractStatusMetadata.New.value, ContractStatusMetadata.Running.value])
                    and (contract['workflow_state'] == ContractWorkflowState.Approved.value)):
                return True, contract

        return False, None

    def get_list_active_contract(self, list_contract):
        list_active_contract = []
        for contract in list_contract:
            if (contract['enable']
                    and (contract['status'] in [ContractStatusMetadata.Running.value])
                    and (contract['workflow_state'] == ContractWorkflowState.Approved.value)
                ):
                list_active_contract.append(contract)

        return list_active_contract

    def get_primary_contract(self, list_contract):
        if (list_contract):
            for contract in list_contract:
                if (contract['is_primary'] and contract['status'] == ContractStatusMetadata.Running.value):
                    return contract

        return None

    def get_hod(self):
        return frappe.get_doc(core.CoreConstants.DoctypeName.Employee, self.head_of_department)

    def get_line_manager(self):
        return frappe.get_doc(core.CoreConstants.DoctypeName.Employee, self.line_manager)

    def get_primary_contract_v2(self, list_contract):
        primary_contract = None

        list_active_contract = self.get_list_active_contract(
            list_contract=list_contract)

        for contract in list_active_contract:
            if ((contract['is_primary'])):
                primary_contract = contract
                break

        return primary_contract, list_active_contract

    def fill_data_for_contract_information(self, list_contract):
        self.contract_info = None
        if (list_contract):
            doc_employee = frappe.get_doc(
                core.CoreConstants.DoctypeName.Employee, self.name)
            # reset contract_info to display new contracts
            doc_employee.contract_info = []
            self.contract_info = None
            for contract in list_contract:
                doc_contract = frappe.get_doc(
                    core.CoreConstants.DoctypeName.Contract, contract['name'])
                doc_employee.append('contract_info', {
                    "code": doc_contract.name,
                    "type": doc_contract.type,
                    "term": doc_contract.term,
                    "amend_to": doc_contract.amend_to,
                    "company": doc_contract.company,
                    "department": doc_contract.department,
                    "job_position": doc_contract.job_position,
                    "start_date": doc_contract.start_date,
                    "end_date": doc_contract.end_date,
                    "is_primary": doc_contract.is_primary,
                    "status": doc_contract.status,
                    "approval_status": doc_contract.workflow_state,
                    "pdf_link": doc_contract.pdf_file,
                    "docx_link": doc_contract.docx_file,
                })
            self.contract_info = doc_employee.contract_info

    def fill_data_for_allowances(self, list_contract):
        self.allowances = None
        if (list_contract):
            doc_employee = frappe.get_doc(
                core.CoreConstants.DoctypeName.Employee, self.name)
            # reset the current allowances to display the latest ones
            doc_employee.allowances = []
            for contract in list_contract:
                doc_contract = frappe.get_doc(
                    core.CoreConstants.DoctypeName.Contract, contract['name'])
                list_allowances = doc_contract.allowances

                for allowance in list_allowances:
                    doc_employee.append("allowances", {
                        "allowance_type": allowance.allowance_type,
                        "apply_on": allowance.apply_on,
                        "start_date": allowance.start_date,
                        "end_date": allowance.end_date,
                        "is_insurance": allowance.is_insurance,
                        "is_pit": allowance.is_pit,
                        "reason": allowance.reason,
                        "amount": allowance.amount,
                    })

            self.allowances = doc_employee.allowances

    def get_company_full_name(self):
        if (self.company):
            return frappe.db.get_value(core.CoreConstants.DoctypeName.Company, self.company, 'full_name')
        return ''

    def set_function_level_from_position_band(self, position_band):
        if (position_band):
            function_level_res = frappe.db.get_value(
                core.CoreConstants.DoctypeName.PositionBand, position_band, 'function_level')
            self.function_level = function_level_res
        else:
            self.function_level = None


    # If the line manager is a HOD or a business representative -> the title is Executive Officer
    # otherwise, the title is Direct Manager
    def get_report_line_title(self, langCode : LanguageCode = LanguageCode.VN):
        list_head_company_department = frappe.db.sql(
            """
                SELECT DISTINCT tD.head_of_department as code
                FROM tabDepartment as tD
                LEFT OUTER JOIN `tabBusiness Representative` as tBR
                ON tD.head_of_department = tBR.br_name
                INNER JOIN tabEmployee as tE
                ON tD.head_of_department = tE.name
            """,
            as_dict = True,
        )

        print('list_head_company_department')
        print(list_head_company_department)

        if (not list_head_company_department):
            return None

        if (not self.line_manager):
            return None

        is_head_company_department = False
        for i in range(len(list_head_company_department)):
            if (list_head_company_department[i]['code'] == self.line_manager):
                is_head_company_department = True
                break

        if (is_head_company_department):
            return frappe._(msg=ReportLineTitle.ExecutiveOfficerEN.value, lang=langCode.value)

        return frappe._(msg=ReportLineTitle.DirectManagerEN.value, lang=langCode.value)


    def get_list_events_with_type(self, event_type : str,):
        values = {
            "event_type": event_type,
            "employee_id": self.name,
        }
        list_events = frappe.db.sql(
            """
                select tCE.employee, tCE.parent
                from `tabCollection Employee` as tCE
                inner join (
                    select tE.name
                    from tabEvents as tE
                    where tE.type = %(event_type)s
                ) as tEOnboard
                where tCE.parenttype = 'Events' and tCE.employee = %(employee_id)s and tEOnboard.name = tCE.parent;
            """,
            values=values,
            as_dict=True,
        )
        print('get_list_events_with_type')
        print(list_events)
        return list_events


    def init_form(self):
        list_contract = self.get_list_contract()

        primary_contract, list_active_contract = self.get_primary_contract_v2(
            list_contract=list_contract)

        print(f'init form is primary contract {primary_contract}')

        if (primary_contract):
            self.company = primary_contract['company']
            self.department = primary_contract['department']
            self.job_position = primary_contract['job_position']
            self.position_band = primary_contract['position_band']
            self.set_function_level_from_position_band(self.position_band)
            self.hired_date = primary_contract['start_date']

            self.update_company_address(primary_contract['company'])
            self.update_head_of_department(primary_contract['department'])
            self.update_seniority(
                primary_contract['start_date'], primary_contract['end_date'], self.terminate_last_working_date)

        else:
            self.job_position = None
            self.position_band = None
            self.set_function_level_from_position_band(None)
            self.hired_date = None

            # self.update_company_address(None)
            # self.update_head_of_department(None)
            self.update_seniority(None, None, None)

        if (list_contract):
            self.fill_data_for_contract_information(list_contract)
        else:
            self.contract_info = None

        # Only show allowances when there is a running primary contract.
        if (list_active_contract):
            self.fill_data_for_allowances(list_active_contract)
        else:
            self.allowances = None

    def onload(self):
        self.init_form()