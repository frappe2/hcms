import enum

JWT_SCRET_KEY = "hcms"
JWT_ALGORITHM = "HS256"
ROLE_REPLY_OFFER_LETTER = "Candidate"
ROLE_CREATE_OFFER_LETTER = "Talent Acquisition Manager"
EMPLOYEE_WORKFLOW = "Employee Workflow"
OTP_LIFE_DURATION_SECOND = 60


class HCMSAPIDomain(enum.Enum):
    DigitalSignURLDev = "https://digisign-crm-new.masterisehomes.com:10001"
    

class HCMSAPIPath(enum.Enum):
    DigitalCompanySignUrl = "/freeDigiSign_CRM_New_Company_Req"

class EmailTemplate(enum.Enum):
    OfferLetter = 'hcms/templates/emails/offer_letter_template.html'
    OnboardingAgenda = 'hcms/templates/emails/onboarding_agenda.html'

class OfferLetterJWTKeys(enum.Enum):
    Email = "email"
    Name = "name"
    Action = "action"

class OfferLetterReplyActions(enum.Enum):
    Accept = "Accept"
    Deny = "Deny"
    Negotiate = "Negotiate"

class OTPSendingMethod(enum.Enum):
    SMS = "SMS"
    Email = "Email"

class EmployeeWorkflowState(enum.Enum):
    OnboardOffering = "Onboard Offering"
    OnboardOfferAccepting = "Onboard Offer Accepting"
    OnboardOfferAccepted = "Onboard Offer Accepted"
    OnboardOfferDenied = "Onboard Offer Denied"
    OnboardShowingUp = "Onboard Showing Up"
    OnboardAbsence = "Onboard Absence"
    OnboardInfoFilled = "Onboard Info Filled"
    OnboardContractSigned = "Onboard Contract Signed"
    OnboardInProbation = "Onboard In Probation"
    OnboardCancelled = "Onboard Cancelled"
    InterviewCVAssessment = "Interview CV Assessment"
    InterviewUnderInterview = "Interview Under Interview"
    InterviewPassed = "Interview Passed"
    InterviewRejected = "Interview Rejected"

class EmployeeWorkflowAction(enum.Enum):
    Shortlist = "Shortlist"
    Pass = "Pass"
    Reject = "Reject"
    CreateOfferLetter = "Create an offer letter"
    SendOffer = "Send the offer letter"
    ResendOffer = "Resend the offer letter"
    Cancel = "Cancel"
    Absent = "Absent"
    UpdateOfferLetter = "Update Offer Letter"
    ConfirmShowingUp = "Confirm Showing-up"
    SendAgenda = "Send Agenda"
    Submit = "Submit"
    SignContract = "Sign Contract"
    ConfirmInProbation = "Confirm In Probation"

class EmployeeStatusMetadata(enum.Enum):
    Applicant = "Applicant"
    ApplicantJunk = "Applicant Junk"
    Candidate = "Candidate"
    CandidateJunk = "Candidate Junk"
    Preboarding = "Preboarding"
    Onboarding = "Onboarding"
    InProbation = "In Probation"
    Active = "Active"
    OnLeaves = "On Leaves"
    UnderTermination = "Under Termination"
    Offboarding = "Offboarding"

class DeactivationDateMetadata(enum.Enum):
    Immediately = 'Immediately'
    AfterDate = 'After Date'

class CandidateSourceMetadata(enum.Enum):
    HiringWebsitePortal = 'Hiring Website Portal'
    SocialMedia = 'Social Media'
    InternalTransfer = 'Internal Transfer'
    HeadhuntAgency = 'Headhunt Agency'
    Referral = 'Referral'
    Rehire = 'Rehire'
    CobrandedActivities = 'Co-branded Activities'
    OnsiteService = 'On-site Service'

class PersonalDocumentTypeMetadata(enum.Enum):
    CCCD = "Citizen Identity Card/CCCD"
    CMND = "Identity Card/CMND"
    Passport = "Passport"
    DrivingLicense = "Driving License"
    Visa = "Visa"
    WorkPermit = "Work Permit"
    Other = "Other"

class Messages(enum.Enum):
    CheckEmailOTP = 'Please enter the OTP sent to your personal email.'
    OTPExpired = 'The OTP is expired. Please try again to have a new OTP.'
    IncorrectOTP = 'The OTP is incorrect. Please check your personal email to get the latest OTP.'
    NoPrimaryContractFound = 'This employee has no approved primary enable contract'
    MessageOTP = 'MASTERISE - Ma OTP cua ban la'

class OTPSMSServiceEnv(enum.Enum):
    Dev = "TEST"
    Prod = "HCMS"

class ReportLineTitle(enum.Enum):
    ExecutiveOfficerEN = "Executive Officer"
    DirectManagerEN = "Direct Manager"
    ExecutiveOfficerVN = "Giám đốc điều hành"
    DirectManagerVN = "Quản lý trực tiếp"

class LanguageCode(enum.Enum):
    VN = 'vi'
    EN = 'en'

class EmailTemplate(enum.Enum):
    OnboardingAgenda = "Onboarding Agenda"
    OfferLetter = "Offer Letter"